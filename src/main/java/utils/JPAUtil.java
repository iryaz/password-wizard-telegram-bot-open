package utils;

import lombok.extern.slf4j.Slf4j;
import org.jvnet.hk2.annotations.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

@Slf4j
@Service
public class JPAUtil {

    private static final String PERSISTENCE_UNIT = "DEFAULT";

    private static EntityManagerFactory entityManagerFactory;
    private static EntityManager entityManager;

    public static void createEntityManagerFactory() {
        log.debug("Creating entity manager factory");
        try {
            entityManagerFactory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT);
        } catch (Throwable ex) {
            log.error("Initial EntityManagerFactory creation failed. " + ex);
        }
    }

    public static EntityManager getEntityManager() {
//        if(entityManager != null && entityManager.isOpen()){
//            return entityManager;
//        }
        log.debug("Creating new entity manager");
        entityManager = entityManagerFactory.createEntityManager();
        return entityManager;
    }

    public static void close() {
        if(entityManager != null && entityManager.isOpen()){
            closeEntityManager();
        }
        log.debug("Closing entity manager factory");
        entityManagerFactory.close();
    }

    static void closeEntityManager(){
        //log.info("Closing last entity manager");
        entityManager.close();
    }
}
