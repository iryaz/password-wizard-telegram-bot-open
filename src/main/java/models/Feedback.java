package models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name="FEEDBACKS")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Feedback {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long id;

    private int userId;

    private String name;
    private String login;
    private String feedbackText;

    @Override
    public String toString() {
        return name + " : " + feedbackText;
    }
}
