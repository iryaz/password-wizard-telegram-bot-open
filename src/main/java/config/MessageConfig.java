package config;

public class MessageConfig {
    private static final String START_TEXT = "Я генерирую надежные пароли и безопасно храню их! " +
            "Управление осуществляется при помощи меню снизу, что я умею:" +
            "\n 1. Генерация надежных паролей с различными условиями" +
            "\n 2. Надежное хранение сгенерированных или созданных ранее паролей" +
            "\n 3. Безопасное напоминание паролей (с доступом по ПИН коду)" +
            "\n\nПочему я надежен и безопасен?" +
            "\n\nОбмен данными происходит через встроенное в телеграм шифрование, что гарантирует сохранность и конфидециальность ваших данных!" +
            "\n\nВсе сохраненные данные шифруются очень надежно, ведь используется 256-битный ключ в совокупности с вспомогательными алгоритмами.";

    public static final String START_NEW_USER_MESSAGE_TEXT = START_TEXT;
    public static final String START_OLD_USER_MESSAGE_TEXT = START_TEXT;

    public static final String CREATE_PASSWORD_TEXT = "Ниже сгенерирован пароль, скопируйте, используйте и подтвердите!";

    public static final String CREATE_PASSWORD_WITH_SAVE_INPUT_NAME_TEXT = "Сгенерировать пароль с сохранением! Отправьте наименование (до 30, на английском):";
    public static final String CREATE_PASSWORD_WITH_SAVE_INPUT_LOGIN_TEXT = "Сгенерировать пароль с сохранением! Отправьте логин (до 30, на английском):";
    public static final String CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_WAY_TEXT = "Сгенерировать пароль с сохранением! Выбрать способ создания пароля:";
    public static final String CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_AUTO_TEXT = "Ниже сгенерирован пароль, скопируйте, используйте и подтвердите, для сохранения данных!";
    public static final String CREATE_PASSWORD_WITH_SAVE_PASSWORD_MANUAL_DONE_TEXT = "Ниже введеный пароль, скопируйте, используйте и подтвердите, для сохранения данных!";
    public static final String CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_MESSAGE_DONE = "Пароль сохранен!";
    public static final String CREATE_PASSWORD_WITH_SAVE_PASSWORD_COPIED = "Новый пароль сохранен!";
    public static final String CREATE_PASSWORD_WITH_SAVE_INPUT_ERROR_TEXT = "Неверный формат! Повторите попытку...";

    public static final String SAVED_PASSWORDS = "Список сохраненных паролей:     ";
    public static final String SAVED_PASSWORDS_PASSWORD_TEXT = "Ниже сохраненный пароль, скопируйте, используйте и подтвердите!";
    public static final String SAVED_PASSWORDS_EDIT_CREATE_PASSWORD_WAY_TEXT = "Редактировать пароль! Выбрать способ создания пароля:";
    public static final String SAVED_PASSWORDS_EDIT_CREATE_PASSWORD_AUTO_TEXT = "Ниже сгенерирован пароль, скопируйте, используйте и подтвердите, для сохранения данных!";
    public static final String SAVED_PASSWORDS_EDIT_CREATE_PASSWORD_AUTO_COPIED_TEXT = "Пароль скопирован, данные обновлены!";
    public static final String SAVED_PASSWORDS_EDIT_CREATE_PASSWORD_MANUAL_DONE_TEXT = "Ниже введеный пароль, скопируйте, используйте и подтвердите, для сохранения данных!";
    public static final String SAVED_PASSWORDS_EDIT_CREATE_PASSWORD_MANUAL_COPIED_TEXT = "Пароль скопирован, данные обновлены!";
    public static final String SAVED_PASSWORDS_DELETE_TEXT = "Список наименований:";

    public static final String FEEDBACK = "Обратная связь! Отправьте сообщение (255):";
    public static final String ABOUT = "PasswordWizard Bot" +
            "\nХранитель и создатель паролей внутри телеграм, использующий современные методы шифрования для хранения!" +
            "\n\nПочему я надежен и безопасен?" +
            "\n\nОбмен данными происходит через встроенное в телеграм шифрование, что гарантирует сохранность и конфидециальность ваших данных!" +
            "\n\nВсе сохраненные данные шифруются очень надежно, ведь используется 256-битный ключ в совокупности с вспомогательными алгоритмами." +
            "\n\nВаша личная информация принадлежит вам, я не продаю ее и не разглашаю. По возможности, сбор оной сведен к минимуму.";
}
