package data;

import com.google.common.collect.Lists;
import config.ButtonConfig;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class QueryKeyboards {

    public static final List<List<InlineKeyboardButton>> ADMIN = Arrays.asList(
            Lists.newArrayList(
                    new InlineKeyboardButton().setText("Отправить новость").setCallbackData(ButtonConfig.ADMIN_SEND_NEWS)
            ),
            Lists.newArrayList(
                    new InlineKeyboardButton().setText("Статистика").setCallbackData(ButtonConfig.ADMIN_STATISTICS),
                    new InlineKeyboardButton().setText("Обратная связь").setCallbackData(ButtonConfig.ADMIN_FEEDBACKS)
            )
    );

    public static final List<List<InlineKeyboardButton>> CREATE_PASSWORD = Collections.singletonList(
            Lists.newArrayList(
                    new InlineKeyboardButton().setText("Пароль скопирован").setCallbackData(ButtonConfig.CREATE_PASSWORD_COPIED)
            )
    );

    public static final List<List<InlineKeyboardButton>> CREATE_PASSWORD_WITH_SAVE_INPUT_NAME = Collections.singletonList(
            Lists.newArrayList(
                    new InlineKeyboardButton().setText("--- Закрыть ---").setCallbackData(ButtonConfig.CLOSE)
            )
    );

    public static final List<List<InlineKeyboardButton>> CREATE_PASSWORD_WITH_SAVE_INPUT_LOGIN = Collections.singletonList(
            Lists.newArrayList(
                    new InlineKeyboardButton().setText("<-- Назад ---").setCallbackData(ButtonConfig.CREATE_PASSWORD_WITH_SAVE_INPUT_LOGIN_RETURN),
                    new InlineKeyboardButton().setText("--- Закрыть ---").setCallbackData(ButtonConfig.CLOSE)
            )
    );

    public static final List<List<InlineKeyboardButton>> CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_WAY = Arrays.asList(
            Lists.newArrayList(
                    new InlineKeyboardButton().setText("Авто").setCallbackData(ButtonConfig.CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_WAY_AUTO),
                    new InlineKeyboardButton().setText("Ручной").setCallbackData(ButtonConfig.CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_WAY_MANUAL)
            ),
            Lists.newArrayList(
                    new InlineKeyboardButton().setText("Отправить сообщением").setCallbackData(ButtonConfig.CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_WAY_MESSAGE)
            ),
            Lists.newArrayList(
                    //new InlineKeyboardButton().setText("<-- Назад ---").setCallbackData(ButtonConfig.CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_WAY_RETURN),
                    new InlineKeyboardButton().setText("--- Закрыть ---").setCallbackData(ButtonConfig.CLOSE)
            )
    );

    public static final List<List<InlineKeyboardButton>> CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_AUTO = Collections.singletonList(
            Lists.newArrayList(
                    new InlineKeyboardButton().setText("Пароль скопирован").setCallbackData(ButtonConfig.CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_AUTO_COPIED)
            )
    );

    public static final List<List<InlineKeyboardButton>> CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_MANUAL_DONE = Collections.singletonList(
            Lists.newArrayList(
                    new InlineKeyboardButton().setText("Пароль скопирован").setCallbackData(ButtonConfig.CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_MANUAL_COPIED)
            )
    );

    public static final List<List<InlineKeyboardButton>> SAVED_PASSWORDS = Collections.singletonList(
            Lists.newArrayList(
                    new InlineKeyboardButton().setText("--- Закрыть ---").setCallbackData(ButtonConfig.CLOSE)
            )
    );

    public static final List<List<InlineKeyboardButton>> SAVED_PASSWORDS_PASSWORD = Collections.singletonList(
            Lists.newArrayList(
                    new InlineKeyboardButton().setText("--- Скопирован ---").setCallbackData(ButtonConfig.SAVED_PASSWORDS_COPIED)
            )
    );

    public static final List<List<InlineKeyboardButton>> SAVED_PASSWORDS_EDIT_CREATE_PASSWORD_WAY = Arrays.asList(
            Lists.newArrayList(
                    new InlineKeyboardButton().setText("Авто").setCallbackData(ButtonConfig.SAVED_PASSWORDS_EDIT_CREATE_PASSWORD_AUTO),
                    new InlineKeyboardButton().setText("Ручной").setCallbackData(ButtonConfig.SAVED_PASSWORDS_EDIT_CREATE_PASSWORD_MANUAL)
            ),
            Lists.newArrayList(
                    new InlineKeyboardButton().setText("<-- Назад ---").setCallbackData(ButtonConfig.SAVED_PASSWORDS_EDIT_CREATE_PASSWORD_WAY_RETURN),
                    new InlineKeyboardButton().setText("--- Закрыть ---").setCallbackData(ButtonConfig.CLOSE)
            )
    );

    public static final List<List<InlineKeyboardButton>> SAVED_PASSWORDS_EDIT_CREATE_PASSWORD_AUTO = Collections.singletonList(
            Lists.newArrayList(
                    new InlineKeyboardButton().setText("Пароль скопирован").setCallbackData(ButtonConfig.SAVED_PASSWORDS_EDIT_CREATE_PASSWORD_AUTO_COPIED)
            )
    );

    public static final List<List<InlineKeyboardButton>> SAVED_PASSWORDS_EDIT_CREATE_PASSWORD_MANUAL_DONE = Collections.singletonList(
            Lists.newArrayList(
                    new InlineKeyboardButton().setText("Пароль скопирован").setCallbackData(ButtonConfig.SAVED_PASSWORDS_EDIT_CREATE_PASSWORD_MANUAL_COPIED)
            )
    );

    public static final List<List<InlineKeyboardButton>> SETTINGS =  Arrays.asList(
            Lists.newArrayList(
                    new InlineKeyboardButton().setText("ПИН").setCallbackData(ButtonConfig.SETTINGS_PIN_CODE)
            ),
            Lists.newArrayList(
                    new InlineKeyboardButton().setText("--- Закрыть ---").setCallbackData(ButtonConfig.CLOSE)
            )
    );

    public static final List<List<InlineKeyboardButton>> SETTINGS_PIN_CODE_ENABLED =  Arrays.asList(
            Lists.newArrayList(
                    new InlineKeyboardButton().setText("Включить").setCallbackData(ButtonConfig.SETTINGS_PIN_CODE_ENABLED)
            ),
            Lists.newArrayList(
                    new InlineKeyboardButton().setText("--- Закрыть ---").setCallbackData(ButtonConfig.CLOSE)
            )
    );

    public static final List<List<InlineKeyboardButton>> SETTINGS_PIN_CODE_DISABLED =  Arrays.asList(
            Lists.newArrayList(
                    new InlineKeyboardButton().setText("Выключить").setCallbackData(ButtonConfig.SETTINGS_PIN_CODE_DISABLED)
            ),
            Lists.newArrayList(
                    new InlineKeyboardButton().setText("--- Закрыть ---").setCallbackData(ButtonConfig.CLOSE)
            )
    );

    public static final List<List<InlineKeyboardButton>> SETTINGS_PIN_CODE_ENTER = Arrays.asList(
            Lists.newArrayList(
                    new InlineKeyboardButton().setText("      1      ").setCallbackData(ButtonConfig.SETTINGS_PIN_CODE_ENTER + "#1"),
                    new InlineKeyboardButton().setText("      2      ").setCallbackData(ButtonConfig.SETTINGS_PIN_CODE_ENTER + "#2"),
                    new InlineKeyboardButton().setText("      3      ").setCallbackData(ButtonConfig.SETTINGS_PIN_CODE_ENTER + "#3")
            ),
            Lists.newArrayList(
                    new InlineKeyboardButton().setText("      4      ").setCallbackData(ButtonConfig.SETTINGS_PIN_CODE_ENTER + "#4"),
                    new InlineKeyboardButton().setText("      5      ").setCallbackData(ButtonConfig.SETTINGS_PIN_CODE_ENTER + "#5"),
                    new InlineKeyboardButton().setText("      6      ").setCallbackData(ButtonConfig.SETTINGS_PIN_CODE_ENTER + "#6")
            ),
            Lists.newArrayList(
                    new InlineKeyboardButton().setText("      7      ").setCallbackData(ButtonConfig.SETTINGS_PIN_CODE_ENTER + "#7"),
                    new InlineKeyboardButton().setText("      8      ").setCallbackData(ButtonConfig.SETTINGS_PIN_CODE_ENTER + "#8"),
                    new InlineKeyboardButton().setText("      9      ").setCallbackData(ButtonConfig.SETTINGS_PIN_CODE_ENTER + "#9")
            ),
            Lists.newArrayList(
                    new InlineKeyboardButton().setText("Сохранить").setCallbackData(ButtonConfig.SETTINGS_PIN_CODE_ENTER_SAVE),
                    new InlineKeyboardButton().setText("      0      ").setCallbackData(ButtonConfig.SETTINGS_PIN_CODE_ENTER + "#0"),
                    new InlineKeyboardButton().setText("Сброс").setCallbackData(ButtonConfig.SETTINGS_PIN_CODE_ENTER_RESET)
            ),
            Lists.newArrayList(
                    new InlineKeyboardButton().setText("--- Закрыть ---").setCallbackData(ButtonConfig.CLOSE)
            )
    );

    public static final List<List<InlineKeyboardButton>> SETTINGS_PIN_CODE_SAVE = Collections.singletonList(
            Lists.newArrayList(
                    new InlineKeyboardButton().setText("ПИН скопирован").setCallbackData(ButtonConfig.SETTINGS_PIN_CODE_SAVE_COPIED)
            )
    );

    public static final List<List<InlineKeyboardButton>> PIN_CODE_TO_SAVED_PASSWORDS = Arrays.asList(
            Lists.newArrayList(
                    new InlineKeyboardButton().setText("      1      ").setCallbackData(ButtonConfig.PIN_CODE_TO_SAVED_PASSWORDS + "#1"),
                    new InlineKeyboardButton().setText("      2      ").setCallbackData(ButtonConfig.PIN_CODE_TO_SAVED_PASSWORDS + "#2"),
                    new InlineKeyboardButton().setText("      3      ").setCallbackData(ButtonConfig.PIN_CODE_TO_SAVED_PASSWORDS + "#3")
            ),
            Lists.newArrayList(
                    new InlineKeyboardButton().setText("      4      ").setCallbackData(ButtonConfig.PIN_CODE_TO_SAVED_PASSWORDS + "#4"),
                    new InlineKeyboardButton().setText("      5      ").setCallbackData(ButtonConfig.PIN_CODE_TO_SAVED_PASSWORDS + "#5"),
                    new InlineKeyboardButton().setText("      6      ").setCallbackData(ButtonConfig.PIN_CODE_TO_SAVED_PASSWORDS + "#6")
            ),
            Lists.newArrayList(
                    new InlineKeyboardButton().setText("      7      ").setCallbackData(ButtonConfig.PIN_CODE_TO_SAVED_PASSWORDS + "#7"),
                    new InlineKeyboardButton().setText("      8      ").setCallbackData(ButtonConfig.PIN_CODE_TO_SAVED_PASSWORDS + "#8"),
                    new InlineKeyboardButton().setText("      9      ").setCallbackData(ButtonConfig.PIN_CODE_TO_SAVED_PASSWORDS + "#9")
            ),
            Lists.newArrayList(
                    new InlineKeyboardButton().setText("Ввод").setCallbackData(ButtonConfig.PIN_CODE_TO_SAVED_PASSWORDS_ENTER),
                    new InlineKeyboardButton().setText("      0      ").setCallbackData(ButtonConfig.PIN_CODE_TO_SAVED_PASSWORDS + "#0"),
                    new InlineKeyboardButton().setText("Сброс").setCallbackData(ButtonConfig.PIN_CODE_TO_SAVED_PASSWORDS_RESET)
            ),
            Lists.newArrayList(
                    new InlineKeyboardButton().setText("--- Закрыть ---").setCallbackData(ButtonConfig.CLOSE)
            )
    );

    public static final List<List<InlineKeyboardButton>>PIN_CODE_TO_SETTINGS = Arrays.asList(
            Lists.newArrayList(
                    new InlineKeyboardButton().setText("      1      ").setCallbackData(ButtonConfig.PIN_CODE_TO_SETTINGS + "#1"),
                    new InlineKeyboardButton().setText("      2      ").setCallbackData(ButtonConfig.PIN_CODE_TO_SETTINGS + "#2"),
                    new InlineKeyboardButton().setText("      3      ").setCallbackData(ButtonConfig.PIN_CODE_TO_SETTINGS + "#3")
            ),
            Lists.newArrayList(
                    new InlineKeyboardButton().setText("      4      ").setCallbackData(ButtonConfig.PIN_CODE_TO_SETTINGS + "#4"),
                    new InlineKeyboardButton().setText("      5      ").setCallbackData(ButtonConfig.PIN_CODE_TO_SETTINGS + "#5"),
                    new InlineKeyboardButton().setText("      6      ").setCallbackData(ButtonConfig.PIN_CODE_TO_SETTINGS + "#6")
            ),
            Lists.newArrayList(
                    new InlineKeyboardButton().setText("      7      ").setCallbackData(ButtonConfig.PIN_CODE_TO_SETTINGS + "#7"),
                    new InlineKeyboardButton().setText("      8      ").setCallbackData(ButtonConfig.PIN_CODE_TO_SETTINGS + "#8"),
                    new InlineKeyboardButton().setText("      9      ").setCallbackData(ButtonConfig.PIN_CODE_TO_SETTINGS + "#9")
            ),
            Lists.newArrayList(
                    new InlineKeyboardButton().setText("Ввод").setCallbackData(ButtonConfig.PIN_CODE_TO_SETTINGS_ENTER),
                    new InlineKeyboardButton().setText("      0      ").setCallbackData(ButtonConfig.PIN_CODE_TO_SETTINGS + "#0"),
                    new InlineKeyboardButton().setText("Сброс").setCallbackData(ButtonConfig.PIN_CODE_TO_SETTINGS_RESET)
            ),
            Lists.newArrayList(
                    new InlineKeyboardButton().setText("--- Закрыть ---").setCallbackData(ButtonConfig.CLOSE)
            )
    );

    public static final List<List<InlineKeyboardButton>> NULL = Collections.singletonList(Lists.newArrayList());
}
