package models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name="STATISTICS")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Statistic {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long id;

    private int userId;

    private String incomingMessage;
    private boolean hasMessage;
    private boolean hasCallbackQuery;
    private long activityDate;

    @Override
    public String toString() {
        return userId + " : " + incomingMessage + " : " + activityDate;
    }
}