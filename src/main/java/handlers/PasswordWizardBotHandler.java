package handlers;

import com.google.common.collect.Lists;
import config.BotConfig;
import config.ButtonConfig;
import dao.StatisticDaoImpl;
import dao.UserDaoImpl;
import data.ChatActiveKeyboard;
import data.ChatActiveKeyboardData;
import data.MessageRequestData;
import data.UserData;
import lombok.extern.slf4j.Slf4j;
import models.Statistic;
import models.User;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.AnswerCallbackQuery;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.PhotoSize;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import utils.CacheManager;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Slf4j
public class PasswordWizardBotHandler extends TelegramLongPollingBot {
    private TextMessageHandler textMessageHandler = new TextMessageHandler();
    private CallbackQueryHandler callbackQueryHandler = new CallbackQueryHandler();
    private AdminHandler adminHandler = new AdminHandler();

    private long chatId;
    private int messageId;
    private int previousMessageId;

    public PasswordWizardBotHandler(DefaultBotOptions options) {
        super(options);
    }

    @Override
    public String getBotUsername() {
        return BotConfig.CHANNEL_USER;
    }

    @Override
    public String getBotToken() {
        return BotConfig.CHANNEL_TOKEN;
    }

    /**
     * Метод обработки входящих сообщений
     * @param update Входящее сообщение
     */
    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasMessage()) {
            Message message = update.getMessage();
            chatId = message.getChatId();
            messageId = message.getMessageId();

            addAmountMessageFromUserCache(chatId);
            if (isUserBlocked(chatId)) {
                log.debug("User: {} is blocked", chatId);
                return;
            }

            int messageId = message.getMessageId();

            if (CacheManager.chatIdPreviousMessageIdMap.get(chatId) != null) {
                previousMessageId = CacheManager.chatIdPreviousMessageIdMap.get(chatId);
            } else {
                previousMessageId = messageId - 1;
                CacheManager.chatIdPreviousMessageIdMap.put(chatId, previousMessageId);
            }

            if (message.hasText()) {
                textMessageHandler(message);
            }
            else if (message.hasPhoto()) {
                photoMessageHandler(message);
            }
        }
        else if (update.hasCallbackQuery()) {
            CallbackQuery callbackquery = update.getCallbackQuery();
            chatId = update.getCallbackQuery().getMessage().getChatId();
            messageId = update.getCallbackQuery().getMessage().getMessageId();

            callbackQueryHandler(update.getCallbackQuery());
            try {
                sendAnswerCallbackQuery(callbackquery);
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        }
        Runnable r = () -> saveStatistic(update);
        new Thread(r).start();
    }

    /**
     * Обработка входящих текстовых сообщений
     * @param message Объект сообщения
     */
    private void textMessageHandler(Message message) {
        int userId = message.getFrom().getId();
        int messageId = message.getMessageId();
        String incomingMessage = message.getText();

        UserData userData = CacheManager.userDataCache.get(chatId);
        if (Objects.isNull(userData)) {
            userData = new UserData();
        }

        userData.setChatId(chatId);
        {
            userData.setChatId(chatId);
            userData.setUserId(userId);
            userData.setMessageId(messageId);
            userData.setPreviousMessageId(previousMessageId);
            userData.setIncomingMessage(incomingMessage);
            userData.setUserLogin(message.getFrom().getUserName());
            userData.setUserName(Utils.getUserName(message.getFrom()));
        }
        CacheManager.userDataCache.put(chatId, userData);
        textMessageHandler.setData(userData);

        //Обработка ответных сообщений
        switch (message.getText()) {
            case "/start":
            case "/menu": { textMessageHandler.defaultIncomingMessage(); break; }
            case "/admin": { if (userId == 1) { adminHandler.setData(userData); adminHandler.admin(); } break; }
            case "Создать пароль": { textMessageHandler.createPassword(); break; }
            case "С сохранением": { textMessageHandler.createPasswordWithSave(); break; }
            case "Сохраненные" : {
                userData.setPinCode(null);
                userData.setPage(0);
                CacheManager.userDataCache.put(chatId, userData);
                textMessageHandler.getSavedPasswords();
                break;
            }
            case "Обратная связь": { textMessageHandler.feedback(); break; }
            case "Настройки": { textMessageHandler.settings(); break; }
            case "О боте": { textMessageHandler.about(); break; }

            default: {
                ChatActiveKeyboard chatActiveKeyboard;
                ChatActiveKeyboardData chatActiveKeyboardData;
                chatActiveKeyboardData = CacheManager.userDataCache.get(chatId).getChatActiveKeyboardData();
                chatActiveKeyboard = chatActiveKeyboardData.getChatActiveKeyboard();

                if (!message.getText().equals("/start")) {
                    userData.setActiveKeyboardMessageId(chatActiveKeyboardData.getMessageId());
                    userData.setInlineMessageId(chatActiveKeyboardData.getInlineMessageId());
                }
                CacheManager.userDataCache.put(chatId, userData);
                textMessageHandler.setData(userData);

                switch (chatActiveKeyboard) {
                    case CREATE_PASSWORD_WITH_SAVE_INPUT_NAME: { textMessageHandler.handleGeneratePasswordWithSaveName(); break; }
                    case CREATE_PASSWORD_WITH_SAVE_INPUT_LOGIN: { textMessageHandler.handleGeneratePasswordWithSaveLogin(); break; }
                    case CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_MESSAGE: { textMessageHandler.handleCreatePasswordWithSaveCreatePasswordMessage(); break; }
                    case FEEDBACK: { textMessageHandler.saveFeedback(); break; }
                    case ADMIN_SEND_NEWS: {
                        List<SendMessage> sendMessageRequestList = Lists.newArrayList();
                        List<User> userList = new UserDaoImpl().findAll();
                        for (User u : userList) {
                            sendMessageRequestList.add(new SendMessage().setChatId(String.valueOf(u.getId())).setText(incomingMessage));
                        }
                        CacheManager.userDataCache.get(chatId).getMessageRequestData().setSendMessageRequestList(sendMessageRequestList);
                    }
                    default: { break; }
                }
                break;
            }
        }
        sendMessages(message.getChatId());
    }

    private void photoMessageHandler(Message message) {
        ChatActiveKeyboard chatActiveKeyboard;
        ChatActiveKeyboardData chatActiveKeyboardData;
        chatActiveKeyboardData = CacheManager.userDataCache.get(chatId).getChatActiveKeyboardData();
        chatActiveKeyboard = chatActiveKeyboardData.getChatActiveKeyboard();

        if (chatActiveKeyboard == ChatActiveKeyboard.ADMIN_SEND_NEWS) {
            //java.io.File incomingPhoto = downloadPhotoByFilePath(getFilePath(getPhoto(message)));
            String incomingPhoto_id = Objects.requireNonNull(getPhoto(message)).getFileId();
            SendPhoto sendPhoto = new SendPhoto()
                    .setPhoto(incomingPhoto_id)
                    .setCaption(message.getCaption());

            List<User> userList = new UserDaoImpl().findAll();
            for (User u : userList) {
                sendPhoto.setChatId(String.valueOf(u.getId()));
                try {
                    execute(sendPhoto);
                } catch (TelegramApiException e) {
                    log.error("Error with execute method: {}", e.toString());
                }
            }
        }
    }

    /**
     * Обработка нажатий на кнопки клавиатуры
     * @param callbackQuery Объект обратного вызова
     */
    private void callbackQueryHandler(CallbackQuery callbackQuery) {
        int userId = callbackQuery.getFrom().getId();
        String data = callbackQuery.getData();
        long chatId = callbackQuery.getMessage().getChatId();

        UserData userData = null;
        if (!CacheManager.callbackQueryIdUserData.isEmpty() && CacheManager.callbackQueryIdUserData.get(callbackQuery.getId()) != null) {
            userData = CacheManager.callbackQueryIdUserData.get(callbackQuery.getId());
        }

        if (userData == null){
            userData = CacheManager.userDataCache.get(chatId);
        }

        if (userData == null) {
            userData = new UserData();
            data = ButtonConfig.DEFAULT;
        }

        userData.setChatId(chatId);
        userData.setUserId(userId);
        userData.setMessageId(callbackQuery.getMessage().getMessageId());
        userData.setInlineMessageId(callbackQuery.getInlineMessageId());

        CacheManager.userDataCache.put(chatId, userData);
        callbackQueryHandler.setData(userData);

        switch (data) {
            case ButtonConfig.CREATE_PASSWORD_COPIED: { callbackQueryHandler.deleteCopiedPassword(); break; }

            case ButtonConfig.CREATE_PASSWORD_WITH_SAVE_INPUT_LOGIN_RETURN: { callbackQueryHandler.generatePasswordWithSaveName(); break; }
            case ButtonConfig.CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_WAY_RETURN: { callbackQueryHandler.generatePasswordWithSaveLogin(); break; }
            case ButtonConfig.CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_WAY_AUTO: { callbackQueryHandler.generatePasswordWithSavePasswordAuto(); break; }
            case ButtonConfig.CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_WAY_MANUAL: { callbackQueryHandler.generatePasswordWithSavePasswordMessage(); break; }
            case ButtonConfig.CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_AUTO_COPIED:
            case ButtonConfig.CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_MANUAL_COPIED: { callbackQueryHandler.deleteCopiedPasswordWithSave(); break; }

            case ButtonConfig.CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_MANUAL_DONE:
            case ButtonConfig.CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_MANUAL_CAPS_DONE: { callbackQueryHandler.generatePasswordWithSavePasswordManualDone(userData.getManualPassword()); break; }
            case ButtonConfig.CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_MANUAL_RESET:
            case ButtonConfig.CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_MANUAL_CAPS_RESET: { userData.setPinCode(""); callbackQueryHandler.generatePasswordWithSavePasswordManual(); break; }
            case ButtonConfig.CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_MANUAL_CAPS: { callbackQueryHandler.generatePasswordWithSavePasswordManualCaps(userData.getManualPassword()); break; }
            case ButtonConfig.CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_MANUAL_CAPS_CAPS: { callbackQueryHandler.generatePasswordWithSavePasswordManualEnter(userData.getManualPassword()); break; }

            case ButtonConfig.SAVED_PASSWORDS: { textMessageHandler.getSavedPasswords(); break; }
            case ButtonConfig.SAVED_PASSWORDS_NEXT: { callbackQueryHandler.getSavedPasswordsNext(); break; }
            case ButtonConfig.SAVED_PASSWORDS_BACK: { callbackQueryHandler.getSavedPasswordsBack(); break; }
            case ButtonConfig.SAVED_PASSWORDS_COPIED: { callbackQueryHandler.savedPasswordCopied(); break; }
            case ButtonConfig.SAVED_PASSWORDS_EDIT_CREATE_PASSWORD_AUTO: { callbackQueryHandler.savedPasswordsEditCreatePasswordAuto(); break; }
            case ButtonConfig.SAVED_PASSWORDS_EDIT_CREATE_PASSWORD_AUTO_COPIED: { callbackQueryHandler.savedPasswordsEditCreatePasswordAutoCopied(); break; }
            case ButtonConfig.SAVED_PASSWORDS_EDIT_CREATE_PASSWORD_MANUAL: { callbackQueryHandler.savedPasswordsEditCreatePasswordManual(); break; }
            case ButtonConfig.SAVED_PASSWORDS_EDIT_CREATE_PASSWORD_MANUAL_COPIED: { callbackQueryHandler.savedPasswordsEditCreatePasswordManualCopied(); break; }
            case ButtonConfig.SAVED_PASSWORDS_EDIT_CREATE_PASSWORD_WAY_RETURN: { callbackQueryHandler.savedPasswordsEditCreatePasswordWayReturn(); break; }

            case ButtonConfig.SAVED_PASSWORDS_EDIT_CREATE_PASSWORD_MANUAL_DONE:
            case ButtonConfig.SAVED_PASSWORDS_EDIT_CREATE_PASSWORD_MANUAL_CAPS_DONE: { callbackQueryHandler.savedPasswordsEditCreatePasswordManualDone(userData.getManualPassword()); break; }
            case ButtonConfig.SAVED_PASSWORDS_EDIT_CREATE_PASSWORD_MANUAL_RESET:
            case ButtonConfig.SAVED_PASSWORDS_EDIT_CREATE_PASSWORD_MANUAL_CAPS_RESET: { userData.setPinCode(""); callbackQueryHandler.savedPasswordsEditCreatePasswordManual(); break; }
            case ButtonConfig.SAVED_PASSWORDS_EDIT_CREATE_PASSWORD_MANUAL_CAPS: { callbackQueryHandler.savedPasswordsEditCreatePasswordManualCaps(userData.getManualPassword()); break; }
            case ButtonConfig.SAVED_PASSWORDS_EDIT_CREATE_PASSWORD_MANUAL_CAPS_CAPS: { callbackQueryHandler.savedPasswordsEditCreatePasswordManualInput(userData.getManualPassword()); break; }

            case ButtonConfig.SETTINGS_PIN_CODE: { callbackQueryHandler.settingsPinCode(); break; }
            case ButtonConfig.SETTINGS_PIN_CODE_ENABLED: { callbackQueryHandler.settingsPinCodeEnabled(); break; }
            case ButtonConfig.SETTINGS_PIN_CODE_DISABLED: { callbackQueryHandler.settingsPinCodeDisabled(); break; }
            case ButtonConfig.SETTINGS_PIN_CODE_ENTER_SAVE: { callbackQueryHandler.settingsPinCodeSave(userData.getPinCode()); break; }
            case ButtonConfig.SETTINGS_PIN_CODE_ENTER_RESET: { userData.setPinCode(""); callbackQueryHandler.settingsPinCodeReset(); break; }
            case ButtonConfig.SETTINGS_PIN_CODE_SAVE_COPIED: { callbackQueryHandler.deleteCopiedPinCode(); break; }

            case ButtonConfig.PIN_CODE_TO_SAVED_PASSWORDS_ENTER: { callbackQueryHandler.pinCodeEnter(userData.getPinCode()); break; }
            case ButtonConfig.PIN_CODE_TO_SAVED_PASSWORDS_RESET: { userData.setPinCode(""); callbackQueryHandler.pinCodeReset(); break; }
            case ButtonConfig.PIN_CODE_TO_SETTINGS_ENTER: { callbackQueryHandler.pinToSettingsCodeEnter(userData.getPinCode()); break; }
            case ButtonConfig.PIN_CODE_TO_SETTINGS_RESET: { userData.setPinCode(""); callbackQueryHandler.pinToSettingsCodeReset(); break; }

            case ButtonConfig.ADMIN_SEND_NEWS: { adminHandler.setData(userData); adminHandler.adminSendNews(); break; }
            case ButtonConfig.ADMIN_STATISTICS: { adminHandler.setData(userData); adminHandler.adminStatistics(); break; }
            case ButtonConfig.ADMIN_FEEDBACKS: { adminHandler.setData(userData); adminHandler.adminFeedbacks(); break; }

            case ButtonConfig.CLOSE: { callbackQueryHandler.close(); break; }
            default: { break; }
        }

        if (data.contains("#")) {
            String key = data.substring(data.indexOf("#") + 1);
            data = data.substring(0, data.indexOf("#"));
            switch (data) {
                case ButtonConfig.CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_MANUAL:
                case ButtonConfig.CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_MANUAL_CAPS: { callbackQueryHandler.generatePasswordWithSavePasswordManualEnter(saveManualPasswordToUserData(userData, key).getManualPassword()); break; }
                case ButtonConfig.SAVED_PASSWORDS_EDIT_CREATE_PASSWORD_MANUAL:
                case ButtonConfig.SAVED_PASSWORDS_EDIT_CREATE_PASSWORD_MANUAL_CAPS: { callbackQueryHandler.savedPasswordsEditCreatePasswordManualInput(saveManualPasswordToUserData(userData, key).getManualPassword()); break; }

                case ButtonConfig.SAVED_PASSWORDS_EDIT: {
                    String passwordName = key.substring(0, key.indexOf("#"));
                    String passwordLogin = key.substring(key.indexOf("#") + 1);
                    callbackQueryHandler.savedPasswordEdit(passwordName, passwordLogin);
                    break;
                }

                case ButtonConfig.SAVED_PASSWORDS_DELETE: {
                    String passwordName = key.substring(0, key.indexOf("#"));
                    String passwordLogin = key.substring(key.indexOf("#") + 1);
                    callbackQueryHandler.savedPasswordDelete(passwordName, passwordLogin);
                    break; }

                case ButtonConfig.SAVED_PASSWORDS_PASSWORD: {
                    String passwordName = key.substring(0, key.indexOf("#"));
                    String passwordLogin = key.substring(key.indexOf("#") + 1);
                    callbackQueryHandler.passwordMenuButton(passwordName, passwordLogin);
                    break; }

                case ButtonConfig.SETTINGS_PIN_CODE_ENTER: { callbackQueryHandler.settingsPinCodeInput(savePinCodeToUserData(userData, key).getPinCode()); break; }
                case ButtonConfig.PIN_CODE_TO_SAVED_PASSWORDS: { callbackQueryHandler.pinCodeToSavedPasswordsInput(savePinCodeToUserData(userData, key).getPinCode()); break; }
                case ButtonConfig.PIN_CODE_TO_SETTINGS: { callbackQueryHandler.pinToSettingsCodeInput(savePinCodeToUserData(userData, key).getPinCode()); break; }
            }
        }

        CacheManager.callbackQueryIdUserData.put(callbackQuery.getId(), userData);
        CacheManager.userDataCache.put(chatId, userData);
        sendMessages(callbackQuery.getMessage().getChatId());
    }

    private UserData saveManualPasswordToUserData(UserData userData, String key) {
        if (userData.getManualPassword() == null) { userData.setManualPassword(key);
        } else { userData.setManualPassword(userData.getManualPassword() + key); }
        return userData;
    }

    private UserData savePinCodeToUserData(UserData userData, String key) {
        if (userData.getPinCode() == null) { userData.setPinCode(key);
        } else { userData.setPinCode(userData.getPinCode() + key); }
        return userData;
    }

    private void saveStatistic(Update update) {
        int userId = 0;
        String incomingMessage = "";
        boolean hasMessage = false;
        boolean hasCallbackQuery = false;

        if (update.hasMessage()) {
            Message message = update.getMessage();
            userId = message.getFrom().getId();
            incomingMessage = message.getText();
            hasMessage = true;
        }

        else if (update.hasCallbackQuery()) {
            CallbackQuery callbackQuery = update.getCallbackQuery();
            userId = callbackQuery.getFrom().getId();
            hasCallbackQuery = true;
        }

        Statistic statistic = new Statistic();
        {
            statistic.setUserId(userId);
            statistic.setIncomingMessage(incomingMessage);
            statistic.setHasMessage(hasMessage);
            statistic.setHasCallbackQuery(hasCallbackQuery);
            statistic.setActivityDate(System.currentTimeMillis());
        }
        new StatisticDaoImpl().add(statistic);
    }

    private void sendMessages(long chatId) {
        MessageRequestData messageRequestData = CacheManager.userDataCache.get(chatId).getMessageRequestData();

        List<SendMessage> sendMessageRequestList = messageRequestData.getSendMessageRequestList();
        EditMessageText editMessageRequest = messageRequestData.getEditMessageRequest();
        DeleteMessage deleteMessageRequest = messageRequestData.getDeleteMessageRequest();
        List<DeleteMessage> deleteMessageRequestList = messageRequestData.getDeleteMessageRequestList();

        if (deleteMessageRequest != null) { executeMethodAsync(deleteMessageRequest); }

        if (!deleteMessageRequestList.isEmpty()) { deleteMessageRequestList.forEach(this::executeMethodAsync); }

        if (!sendMessageRequestList.isEmpty()) { sendMessageRequestList.forEach(this::executeMethodAsync); }

        if (editMessageRequest != null) { executeMethodAsync(editMessageRequest); }

        clearMessageRequestData(chatId);
    }

    private <T extends Serializable, Method extends BotApiMethod<T>> void executeMethodAsync(Method method) {
        Runnable r = () -> executeMethod(method);
        new Thread(r).start();
    }

    private <T extends Serializable, Method extends BotApiMethod<T>> void executeMethod(Method method) {
        log.debug("MessageId: " + messageId);
        log.debug("Execute method: {}", method.toString());
        try {
            execute(method);
        } catch (TelegramApiException e) {
            if (e.getMessage().equals("Error deleting message")) {
                DeleteMessage deleteMessage = (DeleteMessage) method;
                executeMethod(new EditMessageText()
                        .setChatId(deleteMessage.getChatId())
                        .setMessageId(deleteMessage.getMessageId())
                        .setText("Сообщение удалено!")
                );
            }
            log.error("Error with execute method: {}", method.toString(), e);
            sendLogToAdmin("Error with execute method: { " + method.toString() + " }" + "\n\n" + e.getLocalizedMessage() + "\n\n" + e.getMessage());
        }
    }

    private PhotoSize getPhoto(Message message) {
        if (message.hasPhoto()) {
            List<PhotoSize> photos = message.getPhoto();

            return photos.stream()
                    .findFirst()
                    .orElse(null);
        }
        return null;
    }

    private void sendAnswerCallbackQuery(CallbackQuery callbackquery) throws TelegramApiException{
        AnswerCallbackQuery answerCallbackQuery = new AnswerCallbackQuery();
        answerCallbackQuery.setCallbackQueryId(callbackquery.getId());
        answerCallbackQuery.setShowAlert(false);
        execute(answerCallbackQuery);
    }

    private void clearMessageRequestData(long chatId) {
        CacheManager.userDataCache.get(chatId).setMessageRequestData(new MessageRequestData());
    }

    private void sendLogToAdmin(String log) {
        executeMethod(new SendMessage().setChatId(1L).setText(log));
    }

    private void sendLogToBlockingUser(Long chatId) {
        executeMethod(new SendMessage().setChatId(chatId).setText("Я передохну некоторое время, от вас пришло слишком много запросов"));
    }

    private boolean isUserBlocked(Long chatId) {
        long remainingBlockingTime;
        Long blockingTime = CacheManager.blockedUserCache.get(chatId);
        log.debug("Blocking time: {}", blockingTime);
        if (Objects.isNull(blockingTime) || blockingTime < 0L) {
            return false;
        } else {
            remainingBlockingTime = System.currentTimeMillis() - blockingTime;
        }
        log.debug("Remaining blocking time: {}", remainingBlockingTime);
        if (remainingBlockingTime >= 0 && remainingBlockingTime < 60000L) {
            return true;
        } else {
            CacheManager.blockedUserCache.put(chatId, -1L);
            return false;
        }
    }

    private void addAmountMessageFromUserCache(Long chatId) {
        clearAmountMessageFromUserCache();
        Integer currentAmount = CacheManager.amountMessageFromUserCache.get(chatId);
        if (Objects.nonNull(currentAmount) && currentAmount == 60) {
            log.info("User: {} was blocking", chatId);
            sendLogToBlockingUser(chatId);
            CacheManager.blockedUserCache.put(chatId, System.currentTimeMillis());
            CacheManager.amountMessageFromUserCache.put(chatId, ++currentAmount);
        } else if (Objects.nonNull(currentAmount)) {
            CacheManager.amountMessageFromUserCache.put(chatId, ++currentAmount);
        } else {
            CacheManager.amountMessageFromUserCache.put(chatId, 1);
        }
    }

    private void clearAmountMessageFromUserCache() {
        long messageFromUserCounterResetTime = CacheManager.messageFromUserCounterResetTime.longValue();
        if (messageFromUserCounterResetTime == 0L) {
            CacheManager.messageFromUserCounterResetTime.set(System.currentTimeMillis());
        } else if (System.currentTimeMillis() - messageFromUserCounterResetTime >= 60000L) {
            CacheManager.amountMessageFromUserCache.clear();
            CacheManager.messageFromUserCounterResetTime.set(System.currentTimeMillis());
        }
    }
}
