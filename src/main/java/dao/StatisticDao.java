package dao;

import models.Statistic;

import java.util.List;

public interface StatisticDao {
    List<Statistic> findByUserId(int userId);
    List<Statistic> findAll();
    void add(Statistic statistic);
}
