package models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="USERS")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    @Id
    private int id;

    private String login;
    private String userName;
    private String pinCode;
    private boolean pinCodeEnabled;
    private long startDate;

    @Override
    public String toString() {
        return id + " : " + login;
    }
}
