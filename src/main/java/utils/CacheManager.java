package utils;

import com.google.common.collect.Maps;
import data.UserData;
import models.Password;
import models.User;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class CacheManager {
    public static final Map<Integer, User> userCache = Maps.newConcurrentMap();
    public static final Map<Long, UserData> userDataCache = Maps.newConcurrentMap();
    public static final Map<Integer, List<Password>> passwordListCache = Maps.newConcurrentMap();
    public static final Map<Long, Integer> chatIdPreviousMessageIdMap = Maps.newConcurrentMap();
    public static final Map<String, UserData> callbackQueryIdUserData = Maps.newConcurrentMap();
    public static final Map<Long, Long> blockedUserCache = Maps.newConcurrentMap();
    public static final Map<Long, Integer> amountMessageFromUserCache = Maps.newConcurrentMap();
    public static final AtomicLong messageFromUserCounterResetTime = new AtomicLong();
}
