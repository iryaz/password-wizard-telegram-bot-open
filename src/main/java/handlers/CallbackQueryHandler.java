package handlers;

import config.MessageConfig;
import data.ChatActiveKeyboard;
import data.ChatActiveKeyboardData;
import data.UserData;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import models.Password;
import models.User;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import utils.CacheManager;
import utils.PasswordEncoder;
import utils.PasswordGenerator;

@Slf4j
@Data
class CallbackQueryHandler {
    private MessageCreator messageCreator = new MessageCreator();
    private PasswordEncoder passwordEncoder = new PasswordEncoder();

    private long chatId;
    private int userId;
    private String inlineMessageId;
    private int messageId;

    void setData(UserData userData) {
        chatId = userData.getChatId();
        userId = userData.getUserId();
        messageId = userData.getMessageId();
        inlineMessageId = userData.getInlineMessageId();
    }

    void pinCodeToSavedPasswordsInput(String enteredPin) {
        setEditMessage(messageCreator.editKeyboardMessage(userId, chatId, messageId, inlineMessageId, enteredPin.replace("", " ").trim(), ChatActiveKeyboard.PIN_CODE_TO_SAVED_PASSWORDS));
    }

    void pinCodeEnter(String pinCode) {
        String messageText;
        User user = Utils.getUser(userId);

        if (pinCode.equals(String.valueOf(user.getPinCode()))) {
            setEditMessage(messageCreator.editKeyboardMessage(userId, chatId, messageId, inlineMessageId, MessageConfig.SAVED_PASSWORDS, ChatActiveKeyboard.SAVED_PASSWORDS));
        } else {
            messageText = "ПИН-код введен НЕверно!";
            setEditMessage(messageCreator.editKeyboardMessage(userId, chatId, messageId, inlineMessageId, messageText, ChatActiveKeyboard.NULL));
        }
        CacheManager.userDataCache.get(chatId).setPinCode("");
    }

    void pinToSettingsCodeReset() {
        setEditMessage(messageCreator.editKeyboardMessage(userId, chatId, messageId, inlineMessageId, "Настройки > Введите ПИН", ChatActiveKeyboard.PIN_CODE_TO_SETTINGS));
    }

    void pinToSettingsCodeInput(String enteredPin) {
        setEditMessage(messageCreator.editKeyboardMessage(userId, chatId, messageId, inlineMessageId, enteredPin.replace("", " ").trim(), ChatActiveKeyboard.PIN_CODE_TO_SETTINGS));
    }

    void pinToSettingsCodeEnter(String pinCode) {
        User user = Utils.getUser(userId);

        if (pinCode.equals(String.valueOf(user.getPinCode()))) {
            setEditMessage(messageCreator.editKeyboardMessage(userId, chatId, messageId, inlineMessageId, "Настройки", ChatActiveKeyboard.SETTINGS));
        } else {
            String messageText = "ПИН-код введен НЕверно!";
            setEditMessage(messageCreator.editKeyboardMessage(userId, chatId, messageId, inlineMessageId, messageText, ChatActiveKeyboard.NULL));
        }
        CacheManager.userDataCache.get(chatId).setPinCode("");
    }

    void pinCodeReset() {
        setEditMessage(messageCreator.editKeyboardMessage(userId, chatId, messageId, inlineMessageId, "Введите ПИН", ChatActiveKeyboard.SETTINGS_PIN_CODE_ENTER));
    }

    void deleteCopiedPassword() {
        setDeleteMessage(messageCreator.deleteThisMessage(chatId, messageId));
    }

    void generatePasswordWithSaveName() {
        String messageText = MessageConfig.CREATE_PASSWORD_WITH_SAVE_INPUT_NAME_TEXT;
        setEditMessage(messageCreator.editKeyboardMessage(userId, chatId, messageId, inlineMessageId,  messageText, ChatActiveKeyboard.CREATE_PASSWORD_WITH_SAVE_INPUT_NAME));
        CacheManager.userDataCache.get(chatId).setChatActiveKeyboardData(new ChatActiveKeyboardData(messageId, inlineMessageId, ChatActiveKeyboard.CREATE_PASSWORD_WITH_SAVE_INPUT_NAME));
    }

    void generatePasswordWithSaveLogin() {
        String messageText = MessageConfig.CREATE_PASSWORD_WITH_SAVE_INPUT_LOGIN_TEXT;
        setEditMessage(messageCreator.editKeyboardMessage(userId, chatId, messageId, inlineMessageId,  messageText, ChatActiveKeyboard.CREATE_PASSWORD_WITH_SAVE_INPUT_LOGIN));
        CacheManager.userDataCache.get(chatId).setChatActiveKeyboardData(new ChatActiveKeyboardData(messageId, inlineMessageId, ChatActiveKeyboard.CREATE_PASSWORD_WITH_SAVE_INPUT_LOGIN));
    }

    void generatePasswordWithSavePasswordAuto() {
        String generatedPassword = generatePassword();
        String messageText = MessageConfig.CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_AUTO_TEXT;
        savePasswordToUserData(generatedPassword);
        setEditMessage(messageCreator.editKeyboardMessage(userId, chatId, messageId, inlineMessageId, messageText, ChatActiveKeyboard.NULL));
        setSendMessage(messageCreator.createKeyboardMessage(userId, chatId, generatedPassword, ChatActiveKeyboard.CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_AUTO));
        CacheManager.userDataCache.get(chatId).setChatActiveKeyboardData(new ChatActiveKeyboardData(messageId, inlineMessageId, ChatActiveKeyboard.CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_AUTO));
    }

    void generatePasswordWithSavePasswordManual() {
        CacheManager.userDataCache.get(chatId).setManualPassword("");
        String messageText = "Сгенерировать пароль с сохранением! Введите пароль:";
        setEditMessage(messageCreator.editKeyboardMessage(userId, chatId, messageId, inlineMessageId, messageText, ChatActiveKeyboard.CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_MANUAL));
    }

    void generatePasswordWithSavePasswordManualEnter(String enteredPassword) {
        if (enteredPassword == null || enteredPassword.isEmpty()) { enteredPassword = "Сгенерировать пароль с сохранением! Введите пароль:"; }
        setEditMessage(messageCreator.editKeyboardMessage(userId, chatId, messageId, inlineMessageId, enteredPassword, ChatActiveKeyboard.CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_MANUAL));
    }

    void generatePasswordWithSavePasswordManualCaps(String enteredPassword) {
        if (enteredPassword == null || enteredPassword.isEmpty()) { enteredPassword = "Сгенерировать пароль с сохранением! Введите пароль:"; }
        setEditMessage(messageCreator.editKeyboardMessage(userId, chatId, messageId, inlineMessageId, enteredPassword, ChatActiveKeyboard.CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_MANUAL_CAPS));
    }

    void generatePasswordWithSavePasswordManualDone(String enteredPassword) {
        String messageText = MessageConfig.CREATE_PASSWORD_WITH_SAVE_PASSWORD_MANUAL_DONE_TEXT;
        savePasswordToUserData(enteredPassword);
        setEditMessage(messageCreator.editKeyboardMessage(userId, chatId, messageId, inlineMessageId, messageText, ChatActiveKeyboard.NULL));
        setSendMessage(messageCreator.createKeyboardMessage(userId, chatId, enteredPassword, ChatActiveKeyboard.CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_MANUAL_DONE));
        CacheManager.userDataCache.get(chatId).setChatActiveKeyboardData(new ChatActiveKeyboardData(messageId, inlineMessageId, ChatActiveKeyboard.CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_MANUAL_DONE));
    }

    void generatePasswordWithSavePasswordMessage() {
        CacheManager.userDataCache.get(chatId).setManualPassword("");
        String messageText = "Сгенерировать пароль с сохранением! Отправьте пароль ответным сообщением (30):";
        setEditMessage(messageCreator.editKeyboardMessage(userId, chatId, messageId, inlineMessageId, messageText, ChatActiveKeyboard.CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_MESSAGE));
        CacheManager.userDataCache.get(chatId).setChatActiveKeyboardData(new ChatActiveKeyboardData(messageId, inlineMessageId, ChatActiveKeyboard.CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_MESSAGE));
    }

    private void savePasswordToUserData(String enteredPassword) {
        Password password = CacheManager.userDataCache.get(chatId).getPassword();
        password.setPassword(PasswordEncoder.encrypt(enteredPassword, "соль"));
        CacheManager.userDataCache.get(chatId).setPassword(password);
    }

    void deleteCopiedPasswordWithSave() {
        Password password = CacheManager.userDataCache.get(chatId).getPassword();
        if (password != null) {
            Utils.addPassword(password);
        }
        setEditMessage(messageCreator.editKeyboardMessage(userId, chatId, messageId, inlineMessageId, MessageConfig.CREATE_PASSWORD_WITH_SAVE_PASSWORD_COPIED, ChatActiveKeyboard.NULL));
    }

    void savedPasswordsEditCreatePasswordAutoCopied() {
        Password password = CacheManager.userDataCache.get(chatId).getPassword();
        if (password != null) { Utils.updatePassword(password); }
        setEditMessage(messageCreator.editKeyboardMessage(userId, chatId, messageId, inlineMessageId, MessageConfig.SAVED_PASSWORDS_EDIT_CREATE_PASSWORD_AUTO_COPIED_TEXT, ChatActiveKeyboard.NULL));
    }

    void savedPasswordsEditCreatePasswordManual() {
        CacheManager.userDataCache.get(chatId).setManualPassword("");
        String messageText = "Редактировать пароль! Введите пароль:";
        setEditMessage(messageCreator.editKeyboardMessage(userId, chatId, messageId, inlineMessageId, messageText, ChatActiveKeyboard.SAVED_PASSWORDS_EDIT_CREATE_PASSWORD_MANUAL));
    }

    void savedPasswordsEditCreatePasswordManualCopied() {
        Password password = CacheManager.userDataCache.get(chatId).getPassword();
        if (password != null) { Utils.updatePassword(password); }
        setEditMessage(messageCreator.editKeyboardMessage(userId, chatId, messageId, inlineMessageId, MessageConfig.SAVED_PASSWORDS_EDIT_CREATE_PASSWORD_MANUAL_COPIED_TEXT, ChatActiveKeyboard.NULL));
    }

    void savedPasswordsEditCreatePasswordWayReturn() {
        setEditMessage(messageCreator.editKeyboardMessage(userId, chatId, messageId, inlineMessageId, MessageConfig.SAVED_PASSWORDS, ChatActiveKeyboard.SAVED_PASSWORDS));
    }

    void savedPasswordsEditCreatePasswordManualCaps(String enteredPassword) {
        if (enteredPassword == null || enteredPassword.isEmpty()) { enteredPassword = "Сгенерировать пароль с сохранением! Введите пароль:"; }
        setEditMessage(messageCreator.editKeyboardMessage(userId, chatId, messageId, inlineMessageId, enteredPassword, ChatActiveKeyboard.SAVED_PASSWORDS_EDIT_CREATE_PASSWORD_MANUAL_CAPS));

    }

    void savedPasswordsEditCreatePasswordManualInput(String enteredPassword) {
        if (enteredPassword == null || enteredPassword.isEmpty()) { enteredPassword = "Редактировать пароль! Введите пароль:"; }
        setEditMessage(messageCreator.editKeyboardMessage(userId, chatId, messageId, inlineMessageId, enteredPassword, ChatActiveKeyboard.SAVED_PASSWORDS_EDIT_CREATE_PASSWORD_MANUAL));
    }

    void savedPasswordsEditCreatePasswordManualDone(String enteredPassword) {
        String messageText = MessageConfig.SAVED_PASSWORDS_EDIT_CREATE_PASSWORD_MANUAL_DONE_TEXT;
        savePasswordToUserData(enteredPassword);
        setEditMessage(messageCreator.editKeyboardMessage(userId, chatId, messageId, inlineMessageId, messageText, ChatActiveKeyboard.NULL));
        setSendMessage(messageCreator.createKeyboardMessage(userId, chatId, enteredPassword, ChatActiveKeyboard.SAVED_PASSWORDS_EDIT_CREATE_PASSWORD_MANUAL_DONE));
        CacheManager.userDataCache.get(chatId).setChatActiveKeyboardData(new ChatActiveKeyboardData(messageId, inlineMessageId, ChatActiveKeyboard.SAVED_PASSWORDS_EDIT_CREATE_PASSWORD_MANUAL_DONE));
    }

    void passwordMenuButton(String passwordName, String passwordLogin) {
        Password password = Utils.getPassword(userId, passwordName, passwordLogin);
        CacheManager.userDataCache.get(chatId).setPassword(password);
        String messageText = "\n" + passwordName + " : " + passwordLogin;
        String decryptedPassword = PasswordEncoder.decrypt(password.getPassword(), String.valueOf(userId));

        setEditMessage(messageCreator.editKeyboardMessage(userId, chatId, messageId, inlineMessageId, MessageConfig.SAVED_PASSWORDS_PASSWORD_TEXT + messageText, ChatActiveKeyboard.NULL));
        CacheManager.userDataCache.get(chatId).getMessageIdToDeletingList().add(messageId);

        setSendMessage(messageCreator.createKeyboardMessage(userId, chatId, decryptedPassword, ChatActiveKeyboard.SAVED_PASSWORDS_PASSWORD));
        CacheManager.userDataCache.get(chatId).setChatActiveKeyboardData(new ChatActiveKeyboardData(messageId, inlineMessageId, ChatActiveKeyboard.SAVED_PASSWORDS_PASSWORD));
    }

    void getSavedPasswordsNext() {
        int currentPage = CacheManager.userDataCache.get(chatId).getPage();
        CacheManager.userDataCache.get(chatId).setPage(++currentPage);
        setEditMessage(messageCreator.editKeyboardMessage(userId, chatId, messageId, inlineMessageId, MessageConfig.SAVED_PASSWORDS, ChatActiveKeyboard.SAVED_PASSWORDS));
    }

    void getSavedPasswordsBack() {
        int currentPage = CacheManager.userDataCache.get(chatId).getPage();
        CacheManager.userDataCache.get(chatId).setPage(--currentPage);
        setEditMessage(messageCreator.editKeyboardMessage(userId, chatId, messageId, inlineMessageId, MessageConfig.SAVED_PASSWORDS, ChatActiveKeyboard.SAVED_PASSWORDS));
    }

    void savedPasswordEdit(String passwordName, String passwordLogin) {
        Password password = Utils.getPassword(userId, passwordName, passwordLogin);
        CacheManager.userDataCache.get(chatId).setPassword(password);
        String messageText = MessageConfig.SAVED_PASSWORDS_EDIT_CREATE_PASSWORD_WAY_TEXT;

        setDeleteMessage(messageCreator.deletePreviosMessage(chatId, messageId));
        setEditMessage(messageCreator.editKeyboardMessage(userId, chatId, messageId, inlineMessageId,  messageText, ChatActiveKeyboard.SAVED_PASSWORDS_EDIT_CREATE_PASSWORD_WAY));
        CacheManager.userDataCache.get(chatId).setChatActiveKeyboardData(new ChatActiveKeyboardData(messageId, inlineMessageId, ChatActiveKeyboard.SAVED_PASSWORDS_EDIT_CREATE_PASSWORD_WAY));
    }

    void savedPasswordDelete(String passwordName, String passwordLogin) {
        Password password = Utils.getPassword(userId, passwordName, passwordLogin);
        Utils.removePassword(password);

        setDeleteMessage(messageCreator.deletePreviosMessage(chatId, messageId));
        setEditMessage(messageCreator.editKeyboardMessage(userId, chatId, messageId, inlineMessageId, MessageConfig.SAVED_PASSWORDS_DELETE_TEXT, ChatActiveKeyboard.SAVED_PASSWORDS));
        CacheManager.userDataCache.get(chatId).setChatActiveKeyboardData(new ChatActiveKeyboardData(messageId, inlineMessageId, ChatActiveKeyboard.SAVED_PASSWORDS));
    }

    void savedPasswordCopied() {
        setDeleteMessage(messageCreator.deletePreviosMessage(chatId, messageId));
        setEditMessage(messageCreator.editKeyboardMessage(userId, chatId, messageId, inlineMessageId, "Пароль скопирован!", ChatActiveKeyboard.NULL));
    }

    void savedPasswordsEditCreatePasswordAuto() {
        String generatedPassword = generatePassword();
        String messageText = MessageConfig.SAVED_PASSWORDS_EDIT_CREATE_PASSWORD_AUTO_TEXT;
        savePasswordToUserData(generatedPassword);

        setEditMessage(messageCreator.editKeyboardMessage(userId, chatId, messageId, inlineMessageId, messageText, ChatActiveKeyboard.NULL));
        setSendMessage(messageCreator.createKeyboardMessage(userId, chatId, generatedPassword, ChatActiveKeyboard.SAVED_PASSWORDS_EDIT_CREATE_PASSWORD_AUTO));
        CacheManager.userDataCache.get(chatId).setChatActiveKeyboardData(new ChatActiveKeyboardData(messageId, inlineMessageId, ChatActiveKeyboard.SAVED_PASSWORDS_EDIT_CREATE_PASSWORD_AUTO));
    }

    void deleteCopiedPinCode() {
        setEditMessage(messageCreator.editKeyboardMessage(userId, chatId, messageId, inlineMessageId, "ПИН успешно сохранен!", ChatActiveKeyboard.NULL));
    }

    void close() {
        setEditMessage(messageCreator.editKeyboardMessage(userId, chatId, messageId, inlineMessageId, "Closed!", ChatActiveKeyboard.NULL));
    }


    void settingsPinCodeInput(String enteredPin) {
        setEditMessage(messageCreator.editKeyboardMessage(userId, chatId, messageId, inlineMessageId, enteredPin.replace("", " ").trim(), ChatActiveKeyboard.SETTINGS_PIN_CODE_ENABLED));
    }


    void settingsPinCodeReset() {
        setEditMessage(messageCreator.editKeyboardMessage(userId, chatId, messageId, inlineMessageId, "Настройки > ПИН > Включить", ChatActiveKeyboard.SETTINGS_PIN_CODE_ENABLED));
    }

    void settingsPinCodeSave(String pinCode) {
        Runnable r = () -> {
            User user = Utils.getUser(userId);
            user.setPinCode(pinCode);
            user.setPinCodeEnabled(true);
            Utils.updateUser(user);
        };
        new Thread(r).start();

        setEditMessage(messageCreator.editKeyboardMessage(userId, chatId, messageId, inlineMessageId, pinCode, ChatActiveKeyboard.SETTINGS_PIN_CODE_SAVE));
        CacheManager.userDataCache.get(chatId).setPinCode("");
    }

    void settingsPinCode() {
        setEditMessage(messageCreator.editKeyboardMessage(userId, chatId, messageId, inlineMessageId, "Настройки > ПИН", ChatActiveKeyboard.SETTINGS_PIN_CODE));
    }

    void settingsPinCodeEnabled() {
        setEditMessage(messageCreator.editKeyboardMessage(userId, chatId, messageId, inlineMessageId, "Настройки > ПИН > Включить", ChatActiveKeyboard.SETTINGS_PIN_CODE_ENABLED));
    }

    void settingsPinCodeDisabled() {
        //String messageText = "ПИН выключен!";

        Runnable r = () -> {
            User user = Utils.getUser(userId);
            user.setPinCode("");
            user.setPinCodeEnabled(false);
            Utils.updateUser(user);
        };
        new Thread(r).start();

        setDeleteMessage(messageCreator.deleteThisMessage(chatId, messageId));
        //setEditMessage(messageCreator.editKeyboardMessage(userId, chatId, messageId, inlineMessageId, messageText, ChatActiveKeyboard.SETTINGS_PIN_CODE_DISABLED));
    }

    private String generatePassword() {
        PasswordGenerator passwordGenerator = new PasswordGenerator.PasswordGeneratorBuilder()
                .useDigits(true)
                .useLower(true)
                .useUpper(true)
                .usePunctuation(false)
                .build();

        return passwordGenerator.generate(15);
    }

    private void setSendMessage(SendMessage sendMessage) {
        CacheManager.userDataCache.get(chatId).getMessageRequestData().getSendMessageRequestList().add(sendMessage);
    }

    private void setEditMessage(EditMessageText editMessageText) {
        CacheManager.userDataCache.get(chatId).getMessageRequestData().setEditMessageRequest(editMessageText);
    }

    private void setDeleteMessage(DeleteMessage deleteMessage) {
        CacheManager.userDataCache.get(chatId).getMessageRequestData().getDeleteMessageRequestList().add(deleteMessage);
    }
}
