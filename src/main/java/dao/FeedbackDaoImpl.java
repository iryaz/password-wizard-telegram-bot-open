package dao;

import models.Feedback;
import utils.JPAUtil;
import utils.SQLManager;

import javax.persistence.EntityManager;
import java.util.List;

public class FeedbackDaoImpl implements FeedbackDao {
    private EntityManager em;

    @Override
    public List<Feedback> findByUserId(int userId) {
        em = JPAUtil.getEntityManager();
        return em.createQuery("SELECT f FROM Feedback f where f.userId = :userId", Feedback.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public List<Feedback> findAll() {
        em = JPAUtil.getEntityManager();
        return em.createQuery("SELECT f FROM Feedback f", Feedback.class)
                .getResultList();
    }

    @Override
    public void add(Feedback feedback) {
        SQLManager.add(feedback);
    }
}
