package dao;

import models.User;

import java.util.List;

public interface UserDao {
    User findById(int UserId);
    List findAll();
    void add(User user);
    void update(User user);
}