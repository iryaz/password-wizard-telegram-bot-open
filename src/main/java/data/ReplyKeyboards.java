package data;

import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ReplyKeyboards {

    public static List<KeyboardRow> MAIN = MAIN();

    private static List<KeyboardRow> MAIN() {
        KeyboardRow keyboardRowFirst = new KeyboardRow();
        keyboardRowFirst.addAll(
                Arrays.asList(
                        new KeyboardButton("Создать пароль"),
                        new KeyboardButton("С сохранением"),
                        new KeyboardButton("Сохраненные")
                )
        );
        KeyboardRow keyboardRowSecond = new KeyboardRow();
        keyboardRowSecond.addAll(
                Arrays.asList(
                        new KeyboardButton("Обратная связь"),
                        new KeyboardButton("Настройки"),
                        new KeyboardButton("О боте")
                )
        );
        return new ArrayList<>(Arrays.asList(
                keyboardRowFirst,
                keyboardRowSecond
        ));
    }
}
