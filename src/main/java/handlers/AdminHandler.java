package handlers;

import dao.FeedbackDaoImpl;
import dao.UserDaoImpl;
import data.ChatActiveKeyboard;
import data.ChatActiveKeyboardData;
import data.UserData;
import models.Feedback;
import models.User;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import utils.CacheManager;

import java.util.List;

class AdminHandler {
    private MessageCreator messageCreator = new MessageCreator();

    private int userId;
    private long chatId;
    private int messageId;
    private int activeKeyboardMessageId;
    private String inlineMessageId;

    void setData(UserData userData) {
        chatId = userData.getChatId();
        userId = userData.getUserId();
        messageId = userData.getMessageId();
        activeKeyboardMessageId = userData.getActiveKeyboardMessageId();
        inlineMessageId = userData.getInlineMessageId();
    }

    void admin() {
        setSendMessage(messageCreator.createKeyboardMessage(userId, chatId, "Админка", ChatActiveKeyboard.ADMIN));
    }

    void adminSendNews() {
        setEditMessage(messageCreator.editKeyboardMessage(userId, chatId, messageId, inlineMessageId, "Отправьте сообщение для рассылки:", ChatActiveKeyboard.ADMIN_SEND_NEWS));
        CacheManager.userDataCache.get(chatId).setChatActiveKeyboardData(new ChatActiveKeyboardData(activeKeyboardMessageId, inlineMessageId, ChatActiveKeyboard.ADMIN_SEND_NEWS));
    }

    void adminStatistics() {
        StringBuilder messageText = new StringBuilder();
        List<User> userList = new UserDaoImpl().findAll();
        for (User u : userList) {
            messageText.append("\nUserId : ").append(u.getId()).append(", ").append("Login : ").append(u.getLogin()).append(", ").append("Name : ").append(u.getUserName());
        }
        setEditMessage(messageCreator.editKeyboardMessage(userId, chatId, messageId, inlineMessageId, messageText.toString(), ChatActiveKeyboard.NULL));
    }

    void adminFeedbacks() {
        StringBuilder messageText = new StringBuilder();
        List<Feedback> feedbackList = new FeedbackDaoImpl().findAll();
        for (Feedback f : feedbackList) {
            messageText.append("\nLogin : ").append(f.getLogin()).append(", ").append("Name : ").append(f.getName()).append(", ").append("Text : ").append(f.getFeedbackText());
        }
        setEditMessage(messageCreator.editKeyboardMessage(userId, chatId, messageId, inlineMessageId, messageText.toString(), ChatActiveKeyboard.NULL));
    }

    private void setSendMessage(SendMessage sendMessage) {
        CacheManager.userDataCache.get(chatId).getMessageRequestData().getSendMessageRequestList().add(sendMessage);
    }

    private void setEditMessage(EditMessageText editMessageText) {
        CacheManager.userDataCache.get(chatId).getMessageRequestData().setEditMessageRequest(editMessageText);
    }
}
