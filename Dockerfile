FROM openjdk:8-jre-alpine
WORKDIR /
RUN mkdir /app
COPY build/libs/password-wizard-telegram-bot-1.0.0.jar /app/password-wizard-telegram-bot-1.0.0.jar
ENTRYPOINT ["/usr/bin/java", "-jar"]
CMD ["/app/password-wizard-telegram-bot-1.0.0.jar", "--server.port=8082"]