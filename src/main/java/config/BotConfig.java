package config;

public class BotConfig {

    private BotConfig() {
        throw new IllegalStateException("BotConfig class");
    }

    public static final String CHANNEL_TOKEN = "token";
    public static final String CHANNEL_USER = "user";

    public static final String PROXY_HOST = "127.0.0.1";
    public static final int PROXY_PORT = 1234;
    public static final String PROXY_USER = "user";
    public static final String PROXY_PASSWORD = "password";
}
