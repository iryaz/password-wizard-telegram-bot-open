package dao;

import models.Password;

import java.util.List;

public interface PasswordDao {
    List<Password> getByUserId(int userId);
    List<Password> getByUserIdAndName(int userId, String name);
    Password getByUserIdAndNameAndLogin(int userId, String name, String login);
    Long getCountByUserId(int userId);
    void add(Password password);
    void update(Password password);
    void remove(Password password);
}
