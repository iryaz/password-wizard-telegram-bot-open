package dao;

import models.Feedback;

import java.util.List;

public interface FeedbackDao {
    List<Feedback> findByUserId(int userId);
    List<Feedback> findAll();
    void add(Feedback feedback);
}
