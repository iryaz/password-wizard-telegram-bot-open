package data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChatActiveKeyboardData {

    int messageId;
    String inlineMessageId;
    ChatActiveKeyboard chatActiveKeyboard;
}
