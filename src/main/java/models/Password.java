package models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name="PASSWORDS")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Password {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long id;

    private int userId;

    private String name;
    private String login;
    private String password;

    @Override
    public String toString() {
        return name + " : " + login;
    }
}


