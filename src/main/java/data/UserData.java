package data;

import com.google.common.collect.Sets;
import lombok.Data;
import models.Feedback;
import models.Password;

import java.util.Set;

@Data
public class UserData {
    private MessageRequestData messageRequestData = new MessageRequestData();
    private ChatActiveKeyboardData chatActiveKeyboardData;

    private long chatId;
    private int userId;
    private int activeKeyboardMessageId;
    private int messageId;
    private Set<Integer> messageIdToDeletingList = Sets.newHashSet();
    private String inlineMessageId;
    private int previousMessageId;
    private String incomingMessage;
    private String userLogin;
    private String userName;
    private Password password;
    private Feedback feedback;

    private long lastMessageTextReceivedTime;
    private String pinCode;
    private String manualPassword;

    private int page;
}
