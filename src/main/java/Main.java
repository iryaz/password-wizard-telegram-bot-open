import config.BotConfig;
import handlers.PasswordWizardBotHandler;
import lombok.extern.slf4j.Slf4j;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.meta.ApiContext;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import utils.JPAUtil;

import java.net.Authenticator;
import java.net.PasswordAuthentication;


@Slf4j
public class Main {

    public static void main(String[] args) {

        Authenticator.setDefault(new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(
                        BotConfig.PROXY_USER,
                        BotConfig.PROXY_PASSWORD.toCharArray());
            }
        });

        ApiContextInitializer.init();

        TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
        DefaultBotOptions botOptions = ApiContext.getInstance(DefaultBotOptions.class);
        {
            botOptions.setProxyHost(BotConfig.PROXY_HOST);
            botOptions.setProxyPort(BotConfig.PROXY_PORT);
        }
        botOptions.setProxyType(DefaultBotOptions.ProxyType.SOCKS5);

        JPAUtil.createEntityManagerFactory();

        Runnable r = () -> {
            try {
                telegramBotsApi.registerBot(new PasswordWizardBotHandler(botOptions));
            } catch (TelegramApiException e) {
                log.error("Error registerBot", e);
            }
        };
        new Thread(r).start();

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            log.info("Service is shutdown...");
            JPAUtil.close();
        }));
    }
}