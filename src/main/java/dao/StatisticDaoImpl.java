package dao;

import models.Statistic;
import utils.JPAUtil;
import utils.SQLManager;

import javax.persistence.EntityManager;
import java.util.List;

public class StatisticDaoImpl implements StatisticDao {
    private EntityManager em;

    @Override
    public List<Statistic> findByUserId(int userId) {
        em = JPAUtil.getEntityManager();
        return em.createQuery("SELECT s FROM Statistic s where s.userId = :userId", Statistic.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public List<Statistic> findAll() {
        em = JPAUtil.getEntityManager();
        return em.createQuery("SELECT s FROM Statistic s", Statistic.class)
                .getResultList();
    }

    @Override
    public void add(Statistic statistic) {
        SQLManager.add(statistic);
    }
}
