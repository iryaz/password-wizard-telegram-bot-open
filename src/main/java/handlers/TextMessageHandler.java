package handlers;

import config.MessageConfig;
import data.ChatActiveKeyboard;
import data.ChatActiveKeyboardData;
import data.UserData;
import lombok.extern.slf4j.Slf4j;
import models.Feedback;
import models.Password;
import models.User;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import utils.CacheManager;
import utils.PasswordEncoder;
import utils.PasswordGenerator;

import java.util.Objects;
import java.util.regex.Pattern;

@Slf4j
class TextMessageHandler {
    private MessageCreator messageCreator = new MessageCreator();

    private int userId;
    private int messageId;
    private String userName;
    private String userLogin;
    private String incomingMessage;
    private String messageText;
    private long chatId;
    private int previousMessageId;
    private int activeKeyboardMessageId;
    private String inlineMessageId;

    private static final String LOWER = "abcdefghijklmnopqrstuvwxyz";
    private static final String UPPER = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String DIGITS = "0123456789";
    private static final String PUNCTUATION = "!@#$%&*()_+-=[]|,./?><";
    private static final String TEXT_REGEXP = LOWER + UPPER + DIGITS + PUNCTUATION;

    void setData(UserData userData) {
        chatId = userData.getChatId();
        userId = userData.getUserId();
        messageId = userData.getMessageId();
        userName = userData.getUserName();
        userLogin = userData.getUserLogin();
        incomingMessage = userData.getIncomingMessage();
        previousMessageId = userData.getPreviousMessageId();
        inlineMessageId = userData.getInlineMessageId();
        activeKeyboardMessageId = userData.getActiveKeyboardMessageId();
    }

    void defaultIncomingMessage() {
        boolean isNewUser = Utils.isNewUser(userId);

        if (isNewUser) {
            messageText = "Приветствую " + userName + "! " + MessageConfig.START_NEW_USER_MESSAGE_TEXT;
            setSendMessage(messageCreator.createKeyboardMessage(userId, chatId, messageText, ChatActiveKeyboard.START));

            Utils.addUser(new User(
                    userId,
                    userLogin,
                    userName,
                    "",
                    false,
                    System.currentTimeMillis()
            ));
        } else {
            messageText = MessageConfig.START_OLD_USER_MESSAGE_TEXT;
            setSendMessage(messageCreator.createKeyboardMessage(userId, chatId, messageText, ChatActiveKeyboard.START));
        }
        CacheManager.userDataCache.get(chatId).setChatActiveKeyboardData(new ChatActiveKeyboardData(activeKeyboardMessageId, inlineMessageId, ChatActiveKeyboard.START));
    }

    void createPassword() {
        String messageText = MessageConfig.CREATE_PASSWORD_TEXT;
        //setSendMessage(messageCreator.createKeyboardMessage(chatId, messageText));
        setSendMessage(messageCreator.createKeyboardMessage(userId, chatId, generatePassword(), ChatActiveKeyboard.CREATE_PASSWORD));
    }

    void createPasswordWithSave() {
        long savedPasswordsSize = Utils.getPasswordsCount(userId);
        log.info(String.valueOf(savedPasswordsSize));
        if (savedPasswordsSize >= 25) {
            setSendMessage(new SendMessage().setChatId(chatId).setText("На данный, вы исчерпали лимит (25) сохраненных паролей, сообщите нам, если вам требуется больше места, через обратную связь или удалите лишнюю запись"));
        } else {
            String messageText = MessageConfig.CREATE_PASSWORD_WITH_SAVE_INPUT_NAME_TEXT;
            setSendMessage(messageCreator.createKeyboardMessage(userId, chatId, messageText, ChatActiveKeyboard.CREATE_PASSWORD_WITH_SAVE_INPUT_NAME));
            CacheManager.userDataCache.get(chatId).setChatActiveKeyboardData(new ChatActiveKeyboardData(activeKeyboardMessageId, inlineMessageId, ChatActiveKeyboard.CREATE_PASSWORD_WITH_SAVE_INPUT_NAME));
        }
    }

    void handleGeneratePasswordWithSaveName() {
        if (incomingMessage.length() < 31 && Pattern.matches(TEXT_REGEXP, incomingMessage)) {
            String messageText = MessageConfig.CREATE_PASSWORD_WITH_SAVE_INPUT_LOGIN_TEXT;
            Password password = new Password();
            {
                password.setUserId(userId);
                password.setName(incomingMessage);
            }
            CacheManager.userDataCache.get(chatId).setPassword(password);

            setSendMessage(messageCreator.createKeyboardMessage(userId, chatId, messageText, ChatActiveKeyboard.CREATE_PASSWORD_WITH_SAVE_INPUT_LOGIN));
            CacheManager.userDataCache.get(chatId).setChatActiveKeyboardData(new ChatActiveKeyboardData(activeKeyboardMessageId, inlineMessageId, ChatActiveKeyboard.CREATE_PASSWORD_WITH_SAVE_INPUT_LOGIN));
        } else {
            messageText = MessageConfig.CREATE_PASSWORD_WITH_SAVE_INPUT_ERROR_TEXT +
                    "\n"+ MessageConfig.CREATE_PASSWORD_WITH_SAVE_INPUT_NAME_TEXT;
            setSendMessage(messageCreator.createKeyboardMessage(userId, chatId, messageText, ChatActiveKeyboard.CREATE_PASSWORD_WITH_SAVE_INPUT_NAME));
        }
    }

    void handleGeneratePasswordWithSaveLogin() {
        setDeleteMessage(messageCreator.deleteThisMessage(chatId, messageId));
        if (incomingMessage.length() < 31 && Pattern.matches(TEXT_REGEXP, incomingMessage)) {
            String messageText = MessageConfig.CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_WAY_TEXT;
            Password password = CacheManager.userDataCache.get(chatId).getPassword();
            {
                password.setLogin(incomingMessage);
            }
            CacheManager.userDataCache.get(chatId).setPassword(password);

            setSendMessage(messageCreator.createKeyboardMessage(userId, chatId, messageText, ChatActiveKeyboard.CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_WAY));
            CacheManager.userDataCache.get(chatId).setChatActiveKeyboardData(new ChatActiveKeyboardData(activeKeyboardMessageId, inlineMessageId, ChatActiveKeyboard.CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_WAY));
        } else {
            messageText = MessageConfig.CREATE_PASSWORD_WITH_SAVE_INPUT_ERROR_TEXT +
                    "\n"+ MessageConfig.CREATE_PASSWORD_WITH_SAVE_INPUT_LOGIN_TEXT;
            setSendMessage(messageCreator.createKeyboardMessage(userId, chatId, messageText, ChatActiveKeyboard.CREATE_PASSWORD_WITH_SAVE_INPUT_LOGIN));
        }
    }

    void handleCreatePasswordWithSaveCreatePasswordMessage() {
        setDeleteMessage(messageCreator.deleteThisMessage(chatId, messageId));
        if (incomingMessage.length() < 31) {
            String messageText = MessageConfig.CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_MESSAGE_DONE;

            Password password = CacheManager.userDataCache.get(chatId).getPassword();
            if (Objects.nonNull(password)) {
                password.setPassword(PasswordEncoder.encrypt(incomingMessage, "соль"));
                Utils.addPassword(password);
            }

            setSendMessage(messageCreator.createKeyboardMessage(userId, chatId, messageText, ChatActiveKeyboard.NULL));
            CacheManager.userDataCache.get(chatId).setChatActiveKeyboardData(new ChatActiveKeyboardData(activeKeyboardMessageId, inlineMessageId, ChatActiveKeyboard.CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_MESSAGE_DONE));
        } else {
            messageText = MessageConfig.CREATE_PASSWORD_WITH_SAVE_INPUT_ERROR_TEXT +
                    "\n"+ "Сгенерировать пароль с сохранением! Отправьте пароль ответным сообщением (до 30):";
            setSendMessage(messageCreator.createKeyboardMessage(userId, chatId, messageText, ChatActiveKeyboard.CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_MESSAGE));
            CacheManager.userDataCache.get(chatId).setChatActiveKeyboardData(new ChatActiveKeyboardData(previousMessageId, inlineMessageId, ChatActiveKeyboard.CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_MESSAGE));
        }
    }

    void feedback() {
        setSendMessage(messageCreator.createKeyboardMessage(userId, chatId, MessageConfig.FEEDBACK, ChatActiveKeyboard.FEEDBACK));
        CacheManager.userDataCache.get(chatId).setChatActiveKeyboardData(new ChatActiveKeyboardData(activeKeyboardMessageId, inlineMessageId, ChatActiveKeyboard.FEEDBACK));
    }

    void saveFeedback() {
        if (incomingMessage.length() < 256) {
            Feedback feedback = new Feedback();
            {
                feedback.setUserId(userId);
                feedback.setLogin(userLogin);
                feedback.setName(userName);
                feedback.setFeedbackText(incomingMessage);
            }
            Utils.addFeedback(feedback);
            setSendMessage(messageCreator.createKeyboardMessage(userId, chatId, "Обратная связь успешно отправлена, большое спасибо!", ChatActiveKeyboard.NULL));
            CacheManager.userDataCache.get(chatId).setChatActiveKeyboardData(new ChatActiveKeyboardData(activeKeyboardMessageId, inlineMessageId, ChatActiveKeyboard.NULL));
        } else {
            setSendMessage(messageCreator.createKeyboardMessage(userId, chatId, "Неверный формат! Повторите попытку...\n" + MessageConfig.FEEDBACK, ChatActiveKeyboard.FEEDBACK));
        }
    }

    void settings() {
        boolean isPinCodeEnabled = Utils.getUser(userId).isPinCodeEnabled();
        if (isPinCodeEnabled) {
            setSendMessage(messageCreator.createKeyboardMessage(userId, chatId, "Настройка ПИН", ChatActiveKeyboard.PIN_CODE_TO_SETTINGS));
        } else {
            setSendMessage(messageCreator.createKeyboardMessage(userId, chatId, "Настройки", ChatActiveKeyboard.SETTINGS));
        }
    }

    void about() {
        setSendMessage(messageCreator.createKeyboardMessage(userId, chatId, MessageConfig.ABOUT, ChatActiveKeyboard.NULL));
    }

    void getSavedPasswords() {
        boolean isPinCodeEnabled = Utils.getUser(userId).isPinCodeEnabled();
        if (isPinCodeEnabled) {
            setSendMessage(messageCreator.createKeyboardMessage(userId, chatId, "Введите ПИН", ChatActiveKeyboard.PIN_CODE_TO_SAVED_PASSWORDS));
        } else {
            setSendMessage(messageCreator.createKeyboardMessage(userId, chatId, MessageConfig.SAVED_PASSWORDS, ChatActiveKeyboard.SAVED_PASSWORDS));
        }
    }

    private String generatePassword() {
        PasswordGenerator passwordGenerator = new PasswordGenerator.PasswordGeneratorBuilder()
                .useDigits(true)
                .useLower(true)
                .useUpper(true)
                .usePunctuation(false)
                .build();

        return passwordGenerator.generate(15);
    }

    private void setSendMessage(SendMessage sendMessage) {
        CacheManager.userDataCache.get(chatId).getMessageRequestData().getSendMessageRequestList().add(sendMessage);
    }

    private void setEditMessage(EditMessageText editMessageText) {
        CacheManager.userDataCache.get(chatId).getMessageRequestData().setEditMessageRequest(editMessageText);
    }

    private void setDeleteMessage(DeleteMessage deleteMessage) {
        CacheManager.userDataCache.get(chatId).getMessageRequestData().setDeleteMessageRequest(deleteMessage);
    }
}
