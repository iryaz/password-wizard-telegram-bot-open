package utils;

import lombok.extern.slf4j.Slf4j;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceException;

@Slf4j
public class SQLManager {

    public static void add(Object object) {
        EntityManager em = JPAUtil.getEntityManager();
        EntityTransaction et = em.getTransaction();
        et.begin();

        if (object != null) {
            log.debug("Add " + object.getClass().getSimpleName() + " to db");
            try {
                em.persist(object);
                em.getTransaction().commit();
            } catch (PersistenceException ex) {
                em.getTransaction().rollback();
                log.error("Failed to add " + object.getClass().getSimpleName() + " to db", ex);
            } finally {
                JPAUtil.closeEntityManager();
            }
        } else {
            JPAUtil.closeEntityManager();
        }
    }

    public static void update(Object object) {
        EntityManager em = JPAUtil.getEntityManager();
        EntityTransaction et = em.getTransaction();
        et.begin();

        if (object != null) {
            log.debug("Update " + object.getClass().getSimpleName() + " in db");
            try {
                em.merge(object);
                et.commit();
            } catch (PersistenceException ex) {
                et.rollback();
                log.error("Failed to update " + object.getClass().getSimpleName() + " in db", ex);
            } finally {
                JPAUtil.closeEntityManager();
            }
        } else {
            JPAUtil.closeEntityManager();
        }
    }

    public static void remove(Object object) {
        EntityManager em = JPAUtil.getEntityManager();
        EntityTransaction et = em.getTransaction();
        et.begin();

        if (object != null) {
            log.debug("Remove " + object.getClass().getSimpleName() + " from db");
            try {
                em.remove(em.contains(object) ? object : em.merge(object));
                et.commit();
            } catch (PersistenceException ex) {
                et.rollback();
                log.error("Failed to remove " + object.getClass().getSimpleName() + " from db", ex);
            } finally {
                JPAUtil.closeEntityManager();
            }
        } else {
            JPAUtil.closeEntityManager();
        }
    }
}