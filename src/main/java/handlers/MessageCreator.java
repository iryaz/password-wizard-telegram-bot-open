package handlers;

import com.google.common.collect.Lists;
import config.ButtonConfig;
import data.ChatActiveKeyboard;
import data.QueryKeyboards;
import data.ReplyKeyboards;
import lombok.extern.slf4j.Slf4j;
import models.Password;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import utils.CacheManager;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;

@Slf4j
class MessageCreator {

    SendMessage createKeyboardMessage(int userId, long chatId, String messageText, ChatActiveKeyboard keyboard) {
        log.info("Sending " + keyboard + " message to chatId: {}", chatId);
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();

        switch (keyboard) {
            case START: { replyKeyboardMarkup.setResizeKeyboard(true).setKeyboard(ReplyKeyboards.MAIN); break; }
            case ADMIN: { inlineKeyboardMarkup.setKeyboard(QueryKeyboards.ADMIN); break; }

            case CREATE_PASSWORD: { inlineKeyboardMarkup.setKeyboard(QueryKeyboards.CREATE_PASSWORD); break; }
            case CREATE_PASSWORD_WITH_SAVE_INPUT_NAME: { inlineKeyboardMarkup.setKeyboard(QueryKeyboards.NULL); break; }
            case CREATE_PASSWORD_WITH_SAVE_INPUT_LOGIN: { inlineKeyboardMarkup.setKeyboard(QueryKeyboards.NULL); break; }
            case CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_WAY: { inlineKeyboardMarkup.setKeyboard(QueryKeyboards.CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_WAY); break;}
            case CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_AUTO: { inlineKeyboardMarkup.setKeyboard(QueryKeyboards.CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_AUTO); break; }
            case CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_MESSAGE: { inlineKeyboardMarkup.setKeyboard(QueryKeyboards.NULL); break; }
            case CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_MANUAL_DONE: { inlineKeyboardMarkup.setKeyboard(QueryKeyboards.CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_MANUAL_DONE); break; }

            case SAVED_PASSWORDS: { inlineKeyboardMarkup.setKeyboard(getSavedPasswords(userId, chatId)); break; }
            case SAVED_PASSWORDS_PASSWORD: { inlineKeyboardMarkup.setKeyboard(getSavedPassword(CacheManager.userDataCache.get(chatId).getPassword())); break; }
            case SAVED_PASSWORDS_EDIT_CREATE_PASSWORD_AUTO: { inlineKeyboardMarkup.setKeyboard(QueryKeyboards.SAVED_PASSWORDS_EDIT_CREATE_PASSWORD_AUTO); break; }
            case SAVED_PASSWORDS_EDIT_CREATE_PASSWORD_MANUAL_DONE: { inlineKeyboardMarkup.setKeyboard(QueryKeyboards.SAVED_PASSWORDS_EDIT_CREATE_PASSWORD_MANUAL_DONE); break; }

            case FEEDBACK: { inlineKeyboardMarkup.setKeyboard(QueryKeyboards.NULL); break; }
            case SETTINGS: { inlineKeyboardMarkup.setKeyboard(QueryKeyboards.SETTINGS); break; }

            case PIN_CODE_TO_SAVED_PASSWORDS: { inlineKeyboardMarkup.setKeyboard(QueryKeyboards.PIN_CODE_TO_SAVED_PASSWORDS); break; }
            case PIN_CODE_TO_SETTINGS: { inlineKeyboardMarkup.setKeyboard(QueryKeyboards.PIN_CODE_TO_SETTINGS); break; }
            default: { break; }
        }

        if (inlineKeyboardMarkup.getKeyboard().size() > 0) {
            return createSendMessage(chatId, messageText, inlineKeyboardMarkup);
        } else if (replyKeyboardMarkup.getKeyboard().size() > 0) {
            return createSendMessage(chatId, messageText, replyKeyboardMarkup);
        } else {
            return createSendMessage(chatId, messageText);
        }
    }

    SendMessage createKeyboardMessage(long chatId, String messageText) {
        log.debug("Sending message: {} to chatId: {}", messageText, chatId);
        return createSendMessage(chatId, messageText);
    }

    EditMessageText editKeyboardMessage(int userId, long chatId, int messageId, String inlineMessageId, String messageText, ChatActiveKeyboard keyboard) {
        log.debug("Editing " + keyboard + " message: {} to chatId: {}", messageText, chatId);
        EditMessageText editMessageText;
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();

        switch (keyboard) {
            case CREATE_PASSWORD_WITH_SAVE_INPUT_NAME: { inlineKeyboardMarkup.setKeyboard(QueryKeyboards.NULL); break; }
            case CREATE_PASSWORD_WITH_SAVE_INPUT_LOGIN: { inlineKeyboardMarkup.setKeyboard(QueryKeyboards.NULL); break; }
            case CREATE_PASSWORD_WITH_SAVE_CREATE_PASSWORD_MESSAGE: { inlineKeyboardMarkup.setKeyboard(QueryKeyboards.NULL); break; }

            case SAVED_PASSWORDS: { inlineKeyboardMarkup.setKeyboard(getSavedPasswords(userId, chatId)); break; }
            case SAVED_PASSWORDS_EDIT_CREATE_PASSWORD_WAY: { inlineKeyboardMarkup.setKeyboard(getSavedPasswordEditCreatePasswordWay(CacheManager.userDataCache.get(chatId).getPassword())); break; }

            case SETTINGS: { inlineKeyboardMarkup.setKeyboard(QueryKeyboards.SETTINGS); break; }
            case SETTINGS_PIN_CODE: { inlineKeyboardMarkup.setKeyboard(getSettingsPinCodeMenu(userId)); break; }
            case SETTINGS_PIN_CODE_ENABLED: { inlineKeyboardMarkup.setKeyboard(QueryKeyboards.SETTINGS_PIN_CODE_ENTER); break; }
            case SETTINGS_PIN_CODE_DISABLED: { inlineKeyboardMarkup.setKeyboard(QueryKeyboards.SETTINGS); break; }
            case SETTINGS_PIN_CODE_ENTER: { inlineKeyboardMarkup.setKeyboard(QueryKeyboards.SETTINGS_PIN_CODE_ENTER); break; }
            case SETTINGS_PIN_CODE_SAVE: { inlineKeyboardMarkup.setKeyboard(QueryKeyboards.SETTINGS_PIN_CODE_SAVE); break; }

            case PIN_CODE_TO_SETTINGS: { inlineKeyboardMarkup.setKeyboard(QueryKeyboards.PIN_CODE_TO_SETTINGS); break; }
            case PIN_CODE_TO_SAVED_PASSWORDS: { inlineKeyboardMarkup.setKeyboard(QueryKeyboards.PIN_CODE_TO_SAVED_PASSWORDS); break; }

            case NULL: { inlineKeyboardMarkup.setKeyboard(QueryKeyboards.NULL); break; }
            default: { break; }
        }

        if (Objects.isNull(messageText)) {
            editMessageText = createEditMessageText(chatId, messageId, inlineMessageId, inlineKeyboardMarkup);
        } else {
            editMessageText = createEditMessageText(chatId, messageId, messageText, inlineMessageId, inlineKeyboardMarkup);
        }

        return editMessageText;
    }

    private List<List<InlineKeyboardButton>> getSavedPasswords(int userId, long chatId) {
        List<Password> passwordList = Utils.getPasswords(userId);
        passwordList.sort(Comparator.comparing(Password::getName));
        List<List<InlineKeyboardButton>> passwordListKeyboard = Lists.newArrayList(QueryKeyboards.SAVED_PASSWORDS);

        int page = CacheManager.userDataCache.get(chatId).getPage();
        passwordList.stream()
                .skip(page * 10)
                .limit(10)
                .forEach(
                    e -> passwordListKeyboard.add(passwordListKeyboard.size() - 1, Lists.newArrayList(
                            new InlineKeyboardButton()
                                    .setText(e.getName() + " : " + e.getLogin())
                                    .setCallbackData(ButtonConfig.SAVED_PASSWORDS_PASSWORD + "#" + e.getName() + "#" + e.getLogin())
                    ))
        );

        if (page == 0 && passwordList.size() > 10) {
            passwordListKeyboard.add(passwordListKeyboard.size() - 1, Lists.newArrayList(
                    new InlineKeyboardButton()
                            .setText(">>>")
                            .setCallbackData(ButtonConfig.SAVED_PASSWORDS_NEXT)
            ));
        } else if (page > 0 && passwordList.size() > page * 10 + 10) {
            passwordListKeyboard.add(passwordListKeyboard.size() - 1, Lists.newArrayList(
                    new InlineKeyboardButton()
                            .setText( "<<<")
                            .setCallbackData(ButtonConfig.SAVED_PASSWORDS_BACK),
                    new InlineKeyboardButton()
                            .setText(">>>")
                            .setCallbackData(ButtonConfig.SAVED_PASSWORDS_NEXT)
            ));
        } else if (page > 0) {
            passwordListKeyboard.add(passwordListKeyboard.size() - 1, Lists.newArrayList(
                    new InlineKeyboardButton()
                            .setText( "<<<")
                            .setCallbackData(ButtonConfig.SAVED_PASSWORDS_BACK)
            ));
        }

//        passwordList.forEach(
//                e -> passwordListKeyboard.add(0, Lists.newArrayList(
//                        new InlineKeyboardButton()
//                                .setText(e.getName() + " : " + e.getLogin())
//                                .setCallbackData(ButtonConfig.SAVED_PASSWORDS_PASSWORD + "#" + e.getName() + "#" + e.getLogin())
//                )));
        return passwordListKeyboard;
    }

    private List<List<InlineKeyboardButton>> getSavedPassword(Password password) {
        String passwordName = password.getName();
        String passwordLogin = password.getLogin();
        List<List<InlineKeyboardButton>> passwordListKeyboard = Lists.newArrayList(QueryKeyboards.SAVED_PASSWORDS_PASSWORD);
        passwordListKeyboard.add(Lists.newArrayList(
                new InlineKeyboardButton().setText("Редактировать").setCallbackData(ButtonConfig.SAVED_PASSWORDS_EDIT + "#" + passwordName + "#" + passwordLogin),
                new InlineKeyboardButton().setText("Удалить").setCallbackData(ButtonConfig.SAVED_PASSWORDS_DELETE + "#" + passwordName + "#" + passwordLogin)
        ));
        return passwordListKeyboard;
    }

    private List<List<InlineKeyboardButton>> getSavedPasswordEditCreatePasswordWay(Password password) {
        String passwordName = password.getName();
        String passwordLogin = password.getLogin();
        List<List<InlineKeyboardButton>> savedPasswordEditCreatePasswordWayListKeyboard = Lists.newArrayList(QueryKeyboards.SAVED_PASSWORDS_EDIT_CREATE_PASSWORD_WAY);
        if (savedPasswordEditCreatePasswordWayListKeyboard.get(1).size() == 1) {
            savedPasswordEditCreatePasswordWayListKeyboard.get(1).add(0,
                    new InlineKeyboardButton()
                            .setText("<-- Назад ---")
                            .setCallbackData(ButtonConfig.SAVED_PASSWORDS_PASSWORD + "#" + passwordName + "#" + passwordLogin)
            );
        }
        return savedPasswordEditCreatePasswordWayListKeyboard;
    }

    private List<List<InlineKeyboardButton>> getSettingsPinCodeMenu(int userId) {
        boolean isPinCodeEnabled = Utils.getUser(userId).isPinCodeEnabled();
        if (isPinCodeEnabled) {
            return QueryKeyboards.SETTINGS_PIN_CODE_DISABLED;
        } else {
            return QueryKeyboards.SETTINGS_PIN_CODE_ENABLED;
        }
    }

    DeleteMessage deleteThisMessage(long chatId, int messageId) {
        return createDeleteMessage(chatId, messageId);
    }

    DeleteMessage deletePreviosMessage(long chatId, int messageId) {
        int messageIdToDelete = CacheManager.userDataCache.get(chatId).getMessageIdToDeletingList().stream()
                .filter(e -> e < messageId)
                .mapToInt(v -> v)
                .max()
                .orElse(0);
        CacheManager.userDataCache.get(chatId).getMessageIdToDeletingList().remove(messageIdToDelete);
        return createDeleteMessage(chatId, messageIdToDelete);
    }

    private SendMessage createSendMessage(long chatId, String messageText, InlineKeyboardMarkup inlineKeyboardMarkup) {
        return new SendMessage()
                .setChatId(chatId).setText(messageText)
                .setReplyMarkup(inlineKeyboardMarkup);
    }

    private SendMessage createSendMessage(long chatId, String messageText, ReplyKeyboardMarkup replyKeyboardMarkup) {
        return new SendMessage()
                .setChatId(chatId).setText(messageText)
                .setReplyMarkup(replyKeyboardMarkup);
    }

    private SendMessage createSendMessage(long chatId, String messageText) {
        return new SendMessage()
                .setChatId(chatId).setText(messageText);
    }

    private EditMessageText createEditMessageText(long chatId, int messageId, String messageText, String inlineMessageId, InlineKeyboardMarkup inlineKeyboardMarkup) {
        return new EditMessageText()
                .setChatId(chatId).setMessageId(messageId).setText(messageText)
                .setInlineMessageId(inlineMessageId).setReplyMarkup(inlineKeyboardMarkup);
    }

    private EditMessageText createEditMessageText(long chatId, int messageId, String inlineMessageId, InlineKeyboardMarkup inlineKeyboardMarkup) {
        return new EditMessageText()
                .setChatId(chatId).setMessageId(messageId)
                .setInlineMessageId(inlineMessageId).setReplyMarkup(inlineKeyboardMarkup);
    }

    private DeleteMessage createDeleteMessage(long chatId, int messageId) {
        return new DeleteMessage()
                .setChatId(chatId).setMessageId(messageId);
    }
}
