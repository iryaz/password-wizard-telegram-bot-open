package dao;

import models.Password;
import utils.JPAUtil;
import utils.SQLManager;

import javax.persistence.EntityManager;
import java.util.List;

public class PasswordDaoImpl implements PasswordDao {
    private EntityManager em;

    public PasswordDaoImpl() {
        this.em = JPAUtil.getEntityManager();
    }

    @Override
    public List<Password> getByUserId(int userId) {
        return em.createQuery("SELECT p FROM Password p where p.userId = :userId", Password.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public List<Password> getByUserIdAndName(int userId, String name) {
        return em.createQuery("SELECT p FROM Password p where p.userId = :userId and p.name = :name", Password.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .getResultList();
    }

    @Override
    public Password getByUserIdAndNameAndLogin(int userId, String name, String login) {
        return em.createQuery("SELECT p FROM Password p where p.userId = :userId and p.name = :name and p.login = :login", Password.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setParameter("login", login)
                .getSingleResult();
    }

    @Override
    public Long getCountByUserId(int userId) {
        return em.createQuery("SELECT COUNT(p) FROM Password p WHERE p.userId = :userId", Long.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Override
    public void add(Password password) {
        SQLManager.add(password);
    }

    @Override
    public  void update(Password password) {
        SQLManager.update(password);
    }

    @Override
    public void remove(Password password) {
        SQLManager.remove(password);
    }
}
