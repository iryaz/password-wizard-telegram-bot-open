package handlers;

import dao.FeedbackDaoImpl;
import dao.PasswordDaoImpl;
import dao.UserDaoImpl;
import models.Feedback;
import models.Password;
import models.User;
import utils.CacheManager;

import java.util.List;
import java.util.Objects;

public class Utils {
    static String getUserName(org.telegram.telegrambots.meta.api.objects.User user) {
        StringBuilder result = new StringBuilder();
        String userName = user.getUserName();
        String firstName = user.getFirstName();
        String lastName = user.getLastName();

        if (firstName != null) {
            result.append(firstName);
            if (lastName != null) {
                result.append(" ").append(lastName);
            }
        } else {
            result.append(userName);
        }
        return result.toString();
    }

    static boolean isNewUser(int userId) {
        User user = getUser(userId);
        return Objects.isNull(user);
    }

    public static User getUser(int userId) {
        User user = CacheManager.userCache.get(userId);
        if (Objects.nonNull(user)) {
            return user;
        } else {
            return new UserDaoImpl().findById(userId);
        }
    }

    public static void addUser(User user) {
        new UserDaoImpl().add(user);
        CacheManager.userCache.put(user.getId(), user);
    }

    public static void updateUser(User user) {
        new UserDaoImpl().update(user);
        CacheManager.userCache.put(user.getId(), user);
    }

    public static void addPassword(Password password) {
        new PasswordDaoImpl().add(password);
        int userId = password.getUserId();
        List<Password> passwordList = CacheManager.passwordListCache.get(userId);
        if (Objects.nonNull(passwordList)) {
            CacheManager.passwordListCache.get(userId).add(password);
        } else {
            passwordList = new PasswordDaoImpl().getByUserId(userId);
            CacheManager.passwordListCache.put(userId, passwordList);
        }
    }

    public static void updatePassword(Password password) {
        new PasswordDaoImpl().update(password);
        CacheManager.passwordListCache.get(password.getUserId()).forEach(
                p -> {
                    if (p.getId() == password.getId()) {
                        p.setPassword(password.getPassword());
                    }
                }
        );
    }

    public static void removePassword(Password password) {
        new PasswordDaoImpl().remove(password);
        CacheManager.passwordListCache.get(password.getUserId()).removeIf(
                p -> p.getId() == password.getId()
        );
    }

    public static Password getPassword(int userId, String passwordName, String passwordLogin) {
        return new PasswordDaoImpl().getByUserIdAndNameAndLogin(userId, passwordName, passwordLogin);
    }

    public static List<Password> getPasswords(int userId) {
        List<Password> passwordList = CacheManager.passwordListCache.get(userId);
        if (Objects.nonNull(passwordList)) {
            return passwordList;
        } else {
            passwordList = new PasswordDaoImpl().getByUserId(userId);
            CacheManager.passwordListCache.put(userId, passwordList);
            return passwordList;
        }
    }

    public static Long getPasswordsCount(int userId) {
        return new PasswordDaoImpl().getCountByUserId(userId);
    }

    public static Long getPasswordId(int userId) {
        List<Password> passwordList = new PasswordDaoImpl().getByUserId(userId);
        if (passwordList.size() == 0) { return 0L;
        } else { return passwordList.get(0).getId(); }
    }

    public static void addFeedback(Feedback feedback) {
        new FeedbackDaoImpl().add(feedback);
    }
}
