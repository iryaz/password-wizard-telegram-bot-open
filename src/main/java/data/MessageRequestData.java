package data;

import com.google.common.collect.Lists;
import lombok.Data;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;

import java.util.List;

@Data
public class MessageRequestData {
    private List<SendMessage> sendMessageRequestList = Lists.newArrayList();
    private EditMessageText editMessageRequest;
    private DeleteMessage deleteMessageRequest;
    private List<DeleteMessage> deleteMessageRequestList = Lists.newArrayList();
}

