package dao;

import lombok.extern.slf4j.Slf4j;
import models.User;
import utils.JPAUtil;
import utils.SQLManager;

import javax.persistence.EntityManager;
import java.util.List;

@Slf4j
public class UserDaoImpl implements UserDao {

    public UserDaoImpl() {}

    @Override
    public User findById(int id) {
        EntityManager em = JPAUtil.getEntityManager();
        return em.find(User.class, id);
    }

    @Override
    public List<User> findAll() {
        EntityManager em = JPAUtil.getEntityManager();
        return em.createQuery("SELECT u FROM User u", User.class)
                .getResultList();
    }

    @Override
    public void add(User user) {
        SQLManager.add(user);
    }

    @Override
    public  void update(User user) {
        SQLManager.update(user);
    }
}

